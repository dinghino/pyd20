"""Program context setup. """

import sys
from os.path import abspath, dirname, join, sep

ROOT = dirname(abspath(__file__))
SRC = join(ROOT, 'src') + sep

""" Initialize the context of the program into sys.path. """
sys.path.insert(0, SRC)
