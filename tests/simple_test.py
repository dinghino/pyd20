""" Simple testing for basic components. """

# set the context to retrieve everything correctly
import test_context

from game import constants, Dice, Item, Character
from game.components.characters import Abilities, Ability, Purse, Inventory

import unittest
import random

AB_TUPLE = ((10, 0), (8, -1), (14, 2), (20, 5), (6, -2),
            (20, 5), (44, 17), (1, -5), (18, 4), (24, 7))
"""Contains couple of values and what the modifier should be.
Is used to test the correct creation of abilities. """


class AbilityTest(unittest.TestCase):

    def setUp(self):
        """ Try to generate a bunch of abilities using the predefined values
        in AB_TUPLE. """
        self._singleAbility = Ability(name=constants.ABILITIES[0], value=20)
        self._abilities = {}

        for ab in constants.ABILITIES:
            v = random.choice(AB_TUPLE)[0]
            self._abilities[ab] = Ability(name=ab, value=v)

    def test_properties_type(self):
        """Check type of the properties in Ability. <name> should be a
        str.title(). """

        ab = self._singleAbility

        self.assertTrue(type(ab.value) == int)
        self.assertTrue(type(ab.mod) == int)
        self.assertTrue(type(ab.name) == str)
        self.assertTrue(ab.name.istitle())

    def test_single_ability(self):
        """Test Ability creation and mod generation. """
        ability = self._singleAbility
        self.assertEqual(ability.mod, 5,
                         'Error in ability modifier')

    def test_modifiers(self):
        """Check the generate modifiers against what they are supposed to be,
        Checking against AB_TUPLE"""
        for ab in self._abilities:
            ability = self._abilities[ab]
            modifier = next(m[1] for m in AB_TUPLE if m[0] == ability.value)
            self.assertEqual(ability.mod, modifier, 'Mod is wrong')

    def test_update_ability(self):
        """Test the ability update functionality that should update the value
        and the modifier. """
        newValue = 4
        # considering the starting value of 20 -> 24 => mod == +7
        newMod = 7
        ability = self._singleAbility
        ability.update(newValue)
        self.assertEqual(ability.mod, newMod, 'Error in updating')


class DiceTest(unittest.TestCase):

    def setUp(self):
        self._dices = {
            'simple': Dice(),
            'complex': Dice('3d6+3')
        }
        # qty of rolls to execute for each roll test
        self._testRolls = 50
        # list with valid string or int for dice creation
        self._validTestNames = [
            '1d8', 'd6', '2d4', '3d8+7', 4, '12', '10d4+12'
            # to fail the related test add something 'abc'
        ]
        # invalid values for dice creation
        self._invalidTestNames = [
            0, '0', -3, '-8', 'ab', '8+3', '1a7'
        ]

    def test_valid_names(self):
        """Test correctly formatted string or integers for dice creation. """
        for name in self._validTestNames:
            try:
                Dice(name)

            except ValueError, error:
                self.fail('Error in creating a dice!\n>> %s' % error)

    def test_invalid_names(self):
        """Test wrongly formatted strings or integers for dice creation. """

        # TODO: Make this test fail passing a valid name

        with self.assertRaises(ValueError) as dice:

            for name in self._invalidTestNames:
                Dice(name)

        self.assertTrue('Invalid name', dice.exception)

    def test_simple_dice_name(self):
        """Check the name of the simple dice, created with no arguments. """
        self.assertTrue(self._dices['simple'].name == '1d20')

    def test_dices_properties(self):
        """Check the properties type for simple and complex dice. """
        for dice in self._dices:

            dice = self._dices[dice]

            self.assertTrue(type(dice.name) == str)
            self.assertTrue(type(dice.faces) == int)
            self.assertTrue(type(dice.bonus) == int)
            self.assertTrue(type(dice.qty) == int)

    def test_complex_dice_name(self):
        """Test a complex dice name composition. """
        self.assertTrue(self._dices['complex'].name == '3d6+3')

    def test_simple_dice_roll(self):
        """Test rolls for simple dice.
        First try with a single roll, then with multiple
        rolls. """

        rollQty = range(self._testRolls)
        rollQty.pop(0)
        dice = self._dices['simple']

        for i in rollQty:
            # Test a single roll
            roll = dice.roll()
            self.assertGreaterEqual(roll, 1)
            self.assertLessEqual(roll, dice.faces)

        for i in rollQty:
            # Test multiple roll results
            roll = dice.roll(i)
            self.assertGreaterEqual(roll, 1 * i)
            self.assertLessEqual(roll, dice.faces * i)

    def test_complex_dice_roll(self):
        """Test rolls for complex dice.
        First try with a single roll, then with multiple rolls. """

        rollQty = range(self._testRolls)
        rollQty.pop(0)
        dice = self._dices['complex']

        for i in rollQty:
            # roll <i> times the dices
            roll = dice.roll(i)

            exp_min_roll = 1 * dice.qty * i
            exp_max_roll = dice.faces * dice.qty * i
            exp_bonus = dice.bonus * i

            self.assertGreaterEqual(roll, exp_min_roll + exp_bonus,
                                    'complex roll %s: %s not < %s' %
                                    (i, roll, exp_min_roll + exp_bonus))

            self.assertLessEqual(roll, exp_max_roll + exp_bonus,
                                 'complex roll %s: %s not < %s' %
                                 (i, roll, exp_max_roll + exp_bonus))

    def test_getRolls_functionality(self):
        """Check the list of elements in the return value of
        roll(getRolls=True). """

        simple_dice = self._dices['simple']
        complex_dice = self._dices['complex']

        for i in range(self._testRolls):
            simpleRolls = simple_dice.roll(i, getRolls=True)
            complexRolls = complex_dice.roll(i, getRolls=True)

            self.assertEqual(len(simpleRolls), i)
            self.assertEqual(len(complexRolls), i * complex_dice.qty)


if __name__ == '__main__':
    unittest.main()


# TODO: Tests to compile

# For all the modules and classes we should check the properties:
# * Try to write read-only -> raise Error
# * Read properties -> check with wanted output
# For all the methods that return something We should also check that what is
# returned is what we expected it to be for that case

# Item testing:
#   * Create some different items.
#   * Test how the price can be set should be possible to set as
#       * 'X <gold>'
#       * 'X <GP>'
#       * 'X <gp>'
#       * 'X<Gp>'
#       * '{"value":x, "coinType":"<gold>"}' ..?
#   * Check all other properties
#   * Check __str__ return

# Abilities & Ability testing:
#   * Create some Ability
#   * Given a simulated character level, check that
#     Ability <= 18 + race bonuses + (character.level / 4)
#     also there can be only one with such a value
#   * Other test will follow

# Purse & Inventory basic testing:
#   * Create Inventory with maxCapacity
#   * Try to add items in inventory. Try to fill capacity and go on -> should notify and not add
#   * Try to remove items until empty ->  should notify and not remove
#   *

# Character testing:
#   * Create characters from dataset and check that they are in fact built upon
#     those data.
#   * Check fallback for race
#   * Test health and status management
#   * Check actions functionality (sell, buy, attack)
#   *
