'''Main module. for now it's used for testing out functionalities. '''
import random
import context
from game import Character, Item
from game.components.items.equipment import Weapon, Armor


def testingGround():
    # setup some characters
    banker = Character(
        race='human', gameClass='Cleric', fname='Banker', lname='DeBankers',
        int=18, cha=17)

    shopKeeper = Character(
        race='dwarf', gameClass='Bard', fname='John', lname='Booker',
        cha=16, inventorySize=250)

    anton = Character(
        race='Elf', gameClass='Ranger', fname='Anton', lname='Lame',
        armor=3, con=11, dex=18)

    dumbGuy = Character(
        race='Halfling', gameClass='Rogue', fname='Dumby', lname='Dumb')

    guard = Character(
        race='Human', gameClass='Fighter', fname='Mr.', lname='Guard',
        con=17, str=18)

    # list with random characters
    randomChars = [Character() for x in range(10)]
    for c in randomChars:
        doLevelUp = random.random()
        if doLevelUp > 0.5:
            c.levelUp(random.randint(1, 3))

    # combine all the characters
    allChars = [banker, shopKeeper, anton, dumbGuy, guard] + randomChars
    # add some levels and stuff
    anton.levelUp()
    dumbGuy.levelUp()
    guard.levelUp(4)
    banker.purse.add('900 gp')

    # TODO: Refactoring add for purse to accept <str> like '30 gp'

    shopKeeper.inventory.add(Item('Junk', 'Useless junk', '1 sp'), 53)
    shopKeeper.inventory.add(Item('Dirty pants', '...why?', '3 sp'), 17)
    shopKeeper.inventory.add(Weapon('Longsword', 'A cool shiny sword', '15 gp', '1d8 x2'), 10)
    shopKeeper.inventory.add(Armor('Padded armor', 'Simple armor', '5 gp', 1, 8), 10)
    # print out stuff
    for c in allChars:

        savings = c.gameClass.bonus

        print c, '| BAB', c.baseAttackBonus, '| Saving Throws',\
              savings['fortitude'], '~', savings['reflex'], '~', savings['will'],\
              '| size', c.race.size, '(%s)' % c._sizeBonus

    print 'banker\'s money:', banker.purse
    print shopKeeper.inventory

def testLevelUp():
    """Test level up functionality and some sort of character's sheet to check
    everything is working fine. """

    def characterSheet(c):
        savings = c.gameClass.bonus

        string = ''
        string += str(c) + ' | race bonus: %s\n' % c.race.abilityBonus
        string += '-- ABILITIES --\n'
        string += str(c.abilities).replace(', ', '\n') + '\n'
        string += '-- SAVING THROWS --\n'
        string += 'Fortitude %s | Reflexes %s | Will %s' \
                  % (savings['fortitude'], savings['reflex'], savings['will'])

        print string

    c = Character(race='Halfling', gameClass='Rogue', gender='Female')

    characterSheet(c)

    c.levelUp(19)

    characterSheet(c)


if __name__ == '__main__':
    testingGround()
    # testLevelUp()
