from collections import OrderedDict
import json
from os.path import abspath, sep


GAME_RESOURCES = abspath('./src/game/resources')


def createSizeModJson():
    """Create a json containing the constants for the size bonuses assigned
    in function of the character size.

    Because I'm lazy and I know it. Also I need to learn.
    """

    # all the sizes available at character's creation
    SIZES = ('fine', 'diminutive', 'tiny', 'small', 'medium',
             'large', 'huge', 'gargantuan', 'colossal')

    """Generate values. """
    # required values that can't be sorted in any other way.
    required = [0, 1, -1]
    # range spacing from - length of the SIZES to + length, counting every 2
    fullRange = range(-len(SIZES) + 1, len(SIZES) + 1, 2)
    # clear out the fullRange range of values divisible by 3 and join the list
    # with the required values.
    values = sorted(required + [x for x in fullRange if not x % 3 == 0],
    reverse=True)

    """ Combine values and sizes into an OrdereDict. """
    # data storage
    data = OrderedDict({})

    for i in range(len(SIZES)):
        name = SIZES[i]
        data[name] = values[i]

    """Store the data. """
    fw = open(GAME_RESOURCES + sep + 'size_modifiers.json', 'w')
    fw.write(json.dumps(data))
    fw.close()


if __name__ == '__main__':
    createSizeModJson()
