import sqlite3
from os.path import join, isfile, abspath
import logging
import uuid
import time

# TODO: Move the logger somewhere else like in main.py
log = logging.getLogger(__name__)
logging.basicConfig(
    level=logging.WARN,
    format='[%(asctime)s] %(levelname)s '
           '(%(name)s.%(funcName)s) ->\t%(message)s')
ch = logging.StreamHandler()
ch.setLevel(logging.WARN)
log.addHandler(ch)


DATABASE = join(abspath('.'), 'src', 'db.sqlite')


def generate_uuid():
    """
    Generate a (sort of) unique id and return it as string.
    Later this may be used as actual ID of the items (primary key) instead of
    using a progressive ID column.
    """
    return unicode(hex(uuid.uuid4().fields[-1])[:14])


def _test():
    """
    Test the database module, trying to access to the sqlite file.
    """
    log.info('Testing database connection...')
    try:
        db = connect()
        log.info('Database is working')
        db.close()
        log.info('Connection closed.')
    except OSError, error:
        log.critical('Database critical error: %s' % error)
        raise OSError(error)
    except sqlite3.Exception, err:
        log.critical('sqlite exception: %s' % err)


def _generate_slug(string):
    """
    Generate and return a slug for the given string.
    """
    return string.lower().strip().replace(' ', '-')


# check database connection and return the connection if succed or rais OSError
def connect(dbName=None):
    """
    Try to open the database withthe given dbName and return the connection.
    if dbName is not passed it will be set it internally, trying to access a
    *db.sqlite* in the root of the main module.

    :raises: ``OSError`` if file is not found
    """
    dbName = DATABASE if dbName is None else dbName
    # If database file is not a file or does not exists in the defined path
    # raise an error instead of creating it.
    try:
        if isfile(dbName):
            db = sqlite3.connect(dbName)
            log.debug('connected to "%s".' % dbName)
            return db
        else:
            raise OSError('Database file %s cannot be found' % dbName)

    except Exception, e:
        m = 'Error while connecting to "{db}". Error: {e}'
        message = m.format(db=dbName, e=e)
        log.critical(message)
        raise OSError(message)


# get all the headers from a table
def _getHeaders(cursor, tableName):
    """
    Get all the headers for the given table.

    :returns: list with all the headers of the given table
    """

    cursor.execute('SELECT * FROM %s' % tableName)
    return [d[0] for d in cursor.description]


# validate the table name against the tables present into the database
def _validateTableName(db=None, tableName=None):
    # normalize table name
    tableName = tableName.lower().replace(' ', '_').replace('-', '_')
    c = db.cursor()

    q = ("SELECT name from sqlite_master WHERE type='table' AND name='%s';"
         % tableName)
    try:
        name = c.execute(q).fetchone()[0]
        return name
    except TypeError:
        all_tables = "SELECT name from sqlite_master WHERE type='table';"
        all_names = c.execute(all_tables).fetchall()
        all_names = [str(n[0]) for n in all_names]

        raise IndexError('"%s" is not a valid table. should be one of %s'
                         % (tableName, all_names))


# parse the data returned from a database get query into a dictionary
def _parse_db_data(db_data, keys):
    """
    Parse the data retrieved from the database into a python structure
    and return it to the caller.
    """

    retVal = []

    # serialize data from the db into a list of dictionaries
    for item in db_data:
        obj = {}
        for i, key in enumerate(keys):
            obj[key] = item[i]
            if type(obj[key]) is unicode:
                obj[key] = str(obj[key])
            if key is not 'description':
                try:
                    # lists are stored as strings with a comma as separator
                    # between list elements, so we try to split the value and
                    # if the returned list has more than 1 element we consider
                    # it to be a list.
                    # This does not count for the 'description' key that can be
                    # long as it wants and have commas etc in it.
                    maybeList = obj[key].split(',')  # try get lists

                    if len(maybeList) > 1:
                        obj[key] = maybeList
                except AttributeError:
                    pass

        retVal.append(obj)

    return retVal


# serialize a dictionary into tuple of (headers), (value) for table insertion
def _prepare_data_for_post(tableName, tableHeaders, data):
    """
    Prepare a `data` dictionary to be inserted into a table using the table
    `tableName` s.
    The returned tuple contains two tuples, one for the headers and one for the
    values that had a match between tableHeader and data keys.

    Table column and relative value have the same position in the two tuple
    returned, so they can be easily used together.

    :param str tableName: name of the table (logging purposes)
    :param list tableHeaders: headers of the table
    :param dict data: dictionary with the values of the table

    :returns: tuple with `headers` (tuple) and `values` (tuple)
    """
    format_data = []

    for idx, th in enumerate(tableHeaders):
        # Loop all the required columns from the table and try to get
        # the value from the data passed.
        # If a key is missing notify but continue and try with
        # the table default value.
        try:
            v = data[th]
            format_data.append((th, v))

        except KeyError:
            # This happens if the data does not have the value for the
            # currently evaluated tableHeader. this may easily happen
            # but it's not important to us, so we notify and continue
            m = 'Key "{th}" required from "{table}" table is missing '\
                'in the item dictionary.'
            log.debug(m.format(th=th, table=tableName))
            continue

    # Create the headers string ready to be filled with the headers
    # name that have found a match in the item's dictionary
    headers = tuple(t[0] for t in format_data)
    # Create the tuple with the values
    values = tuple(t[1] for t in format_data)

    return (headers, values)


# try to insert a single prepared object into a given table
def _insert_into_table(cursor, tableName, headers, values):
    """
    Try to insert a single dictionary into the table using the provided cursor.
    Validation on the table should be done before calling this function.

    :param cursor: sql cursor for the operation
    :param str tableName: the name of the table
    :param tuple headers: columns to insert into (can be ``list``)
    :param tuple values: matching values to insert (can be ``list``)

    .. note::
       ``headers`` and ``values`` tuples are supposed to be created with
       ``_prepare_data_for_post``

    :returns: * ``str`` result, either *OK* or *FAIL*
              * ``str`` composed sql query for the insert, for logging
    """
    status = 'OK'
    # join the headers as string to be used in the sql query string
    headers_string = ', '.join('`%s`' % h for h in headers)

    # query for inserting new items or replacing existing ones
    sql_query = "INSERT OR REPLACE INTO {t}({h}) VALUES(?{v});"
    # compile the query string correctly
    sql_query = sql_query.format(t=tableName,
                                 h=headers_string,
                                 v=',?' * (len(values) - 1))

    try:
        cursor.execute(sql_query, values)
    except Exception, err:
        log.error('SQLite Exception: %s' % err)
        status = 'FAIL'
    finally:
        return status, sql_query


# try to update an existing object (that has a UUID) into the given table
def _update_into_table(cursor, tableName, uuid, headers, values):
    """
    Attempt to update a given object using it's uuid as reference, with the
    given values in the given columns.

    :param cursor: sqlite cursor
    :param str tableName: Name of the table (for logging purposes)
    :param str uuid: the UUID of the row to update
    :param tuple headers: Tuple with the headers to update
    :param tuple values: Matching tuple with the values to update

    :returns: * ``str`` result, either *OK* or *FAIL*
              * ``str`` composed sql query for the update, for logging
    """

    status = 'OK'

    # query that will be compiled and executed
    sql_query = "UPDATE {table} SET {col_values} WHERE uuid='{uuid}'"

    # Since we are updating an object that for sure has an uuid BUT we do not
    # want to update, change, remove or anything that value, we recreate the
    # tuples for the headers and the values excluding the uuid from both of
    # them
    # find the index of 'uuid' in the headers tuple
    uuid_idx = headers.index('uuid')
    # recreate the tuples excluding the 'uuid' header and the value from values
    headers = tuple(v for i, v in enumerate(headers) if i != uuid_idx)
    values = tuple(v for i, v in enumerate(values) if i != uuid_idx)

    # prepare the string to be injected into the SQL query containing the
    # names of the column to update and the placeholder that will be filled
    # with the execute() method of the cursor using the values tuple.
    values_to_update = ', '.join("`%s`=?" % x for x in headers)

    # Format the query with all the needed variables
    sql_query = sql_query.format(table=tableName,
                                 col_values=values_to_update,
                                 uuid=uuid)

    try:
        cursor.execute(sql_query, values)
    except Exception, err:
        log.error('SQLite Exception: %s' % err)
        status = 'FAIL'
    finally:
        return status, sql_query


# get items from a database table
def get(tableName, value=None, key='slug', match=False):
    """Get an item from the requested table.

       Match the key passed with the passed value in the given tableName.
       If value is ``None`` the function will return all the items in the
       table.
       If <match> is True it will return all the items that contains
       such value, else will try to find a perfect match and return that if
       found.
       objects are returned as a list of dictionaries.

       .. code-block:: python

          # returned objects are dictionaries describing the items

          get('weapon')
          # => all the weapons in a list of dict

          get('weapon', 'sword', match=True)
          # Returns the items of the weapon table that have `sword` in their
          # slug column if they exists.
          # => [<Sword>, <Longsword>,...]

          get('weapon', 'sword')
          # Returns a single dictionary parsed from the weapon table that has
          # the exactly 'sword' as slug, if it exists.
          # => returns <Sword>


       :param str tableName: name of the table to get data from

       :param value: Value to search in the table.
                     Can be any of the valid sqlite datatype or ``None``

       :param str key: key (column header) to match the value with.


       :param bool match: Wether should use a regex (`True`) to match anything
                          containing the *value* string or find a
                          perfect match (`False`)


        :default value: `None`
        :default key: `slug`
        :default match: `False`

       :returns: * ``list`` of ``dict`` is returned if no *value* is passed
                   or if *value* is passed but *match* is False
                 * ``dict`` if a *value* is passed and *match* is ``True``
                 * ``False`` if nothing has been found.

    """

    # open the database or raise OSError if file is not found
    db = connect()

    # Validate the table name or raise KeyError
    table_name = _validateTableName(db, tableName)

    # Create the cursor
    cursor = db.cursor()

    # Define the query string.
    # If value is None query all items, else create a query with the given
    # arguments
    if value is None:
        query = "SELECT * FROM {0}".format(table_name)
    else:
        # If a value is passed try to query the database for it.
        # If the key is 'slug' be sure to format it as a slug text string
        # before matching. This allows to use the item name as value and match
        # it against both name and slug indifferently.
        if key is 'slug':
            value = str(value).lower().replace(' ', '-')

        # add the sql partial match strings to t he value if match is False
        value = value if match is False else '%' + value + '%'

        # create and compile the query string
        query = "SELECT * FROM {table} WHERE {key} {operator} \'{value}\';"
        query = query.format(table=table_name,
                             key=key,
                             operator='=' if match is False else 'LIKE',
                             value=value)

    try:
        cursor.execute(query)

        # Fetch all the matching data from the dictionary.
        # this will be a list of tuples that needs to be serialized into dicts
        db_data = cursor.fetchall()
    except Exception as e:
        log.error(e)
        db.rollback()
        raise e
    finally:
        # If db_data is empty return False so that the user knows that there
        # were no items matching the query
        if len(db_data) is 0:
            message = ('No item with {key} = {value} found in "{table}" table.'
                       .format(key=key, value=value, table=table_name))

            log.info(message)
            return False

        # If data has been found, iterate through it and serialize the
        # objects into dictionaries using the table headers as keys

        # Get the column headers that will be set as keys
        keys = _getHeaders(cursor, table_name)

        # Parse the data retrieved.
        # The function will return a list of dictionaries, one for each row
        # retrieved from the database (db_data) using the table headers
        # as keys

        # Close cursor and database connection
        cursor.close()
        db.close()

    values = _parse_db_data(db_data, keys)

    # If we passed a <value> to search and <match> is false and we want just
    # a single item, return the dictionary containing that item instead
    # of a list with just one item
    if value is not None and match is False:
        values = values[0]

    # log the success of the query
    log.debug('{qt} item(s) found for query "{query}"'.format(qt=len(db_data),
                                                              query=query))

    return values


# post item(s) into the given table, insert or update as needed
def post(tableName, postData):
    """
    Accepts a single ``dict`` or ``list`` and ``tuple`` of dictionaries that
    represent each a single row of the table.

    tries to perform an *insert or replace* for new items and *update* for
    existing items.

    For each dictionary it will check for the presence of the UUID property:

    * If missing it will generate it and add the row
    * If an *uuid* is passed it will check that it exists in the current table
      and, if found, tries to update the row.

    This allow to *post* mixed list of objects to insert and update together.

    Properties that needs to be loaded must have the same key as the column
    name that will store them (doesn't matter if upper or lower case).
    Keys that does not have a match inside the table will be ignored.

    :param str tableName: the table where to post the new object.

    :param postData: postData that needs to be posted to the sql postDatabase.
    :type postData: one of ``dict``, ``list``, ``tuple``

    :returns: ``True`` if operation went through or ``False`` if it encountered
              unexpected errors.

    :raises: * `sqlite3.Exceptions`
             * ``TypeError`` for wrong `postData` type

    """
    # return value for the function. default is False.
    # Will set to True if the sql post is successful.
    completed = False
    init_time = time.time()

    if type(postData) is dict:
        # If the passed postData is a single dictionary create a list with that
        # inside.
        postData = [postData]
    elif type(postData) is tuple:
        # If postData is a tuple create a list out of it
        postData = list(postData)

    # Validate postData type
    if type(postData) is not list:
        m = "Was expecting one of ['dict', 'tuple', 'list']. got %s instead"
        m = m % type(postData)
        log.error(m)
        raise TypeError(m)

    # Validate that each item inside postData is a dictionary to avoid errors
    # while prepping the data for post.
    for i, item in enumerate(postData):
        if type(item) is not dict:
            m = 'Error while processing data for POST:'
            m += " expected <type 'dict'> but found {t} at [{idx}] => {item}"
            m = m.format(idx=i, item=item, t=type(item))
            log.error(m)
            raise TypeError(m)

    total_objects = len(postData)

    db = connect()
    cursor = db.cursor()
    # validate and normalize the table name required
    tableName = _validateTableName(db, tableName)

    # get all the table headers in a list so we can match the postData
    # with the correct dict pairs.
    tableHeaders = _getHeaders(cursor, tableName)
    existing_uuid = getList(tableName, 'uuid')
    try:
        for current, data in enumerate(postData):
            action = 'INSERT'
            # reformat the keys of data all lower case to be sure they match
            # the columns' name. also remove any spaces and change them with a
            # '_' to match the columns
            data = {k.lower().strip().replace(' ', '_'): data[k] for k in data}

            if 'uuid' not in data:
                # if an object does not have an UUID we consider it a new
                # row for the database, so we give it a brand new UUID, and we
                # set the <action> to 'INSERT' so we later know that we
                # want to insert it.
                data['uuid'] = generate_uuid()
                action = 'INSERT'
            else:
                # if the UUID exists in the object we are inserting, check that
                # we have an item with the same UUID in the database. if that
                # is the case we update it.
                if data['uuid'] in existing_uuid:
                    action = 'UPDATE'

            if 'name' in data and 'slug' not in data:
                data['slug'] = _generate_slug(data['name'])

            # prepare the dictionary of the current loop for insertion.
            # headers is string of all the headers that the dictionary had
            # values for, while values is a tuple of all the related values.
            headers, values = _prepare_data_for_post(tableName,
                                                     tableHeaders,
                                                     data)

            if action is 'INSERT':
                # try to insert the object into the given table and get a
                # result string to be used into the log output
                result, query = _insert_into_table(cursor,
                                                   tableName,
                                                   headers,
                                                   values)
            elif action is 'UPDATE':
                # Try to update the object
                result, query = _update_into_table(cursor,
                                                   tableName,
                                                   data['uuid'],
                                                   headers,
                                                   values)

            # Log the outcome of the function
            # Prepare the "column:value, ... " string for the logger
            values_s = zip(headers, values)
            values_string = ', '.join('%s:%s' % (x[0], x[1]) for x in values_s)
            # log the result
            m = '[{curr}/{tot}] [{status}] {action} into "{table}": {values}'
            message = m.format(table=tableName, values=values_string,
                               action=action, status=result,
                               tot=total_objects, curr=current + 1)

            if result is 'FAIL':
                log.error(message)
            else:
                log.info(message)

        # after looping through all the data commit the changes and go on
        db.commit()
        completed = True

    # In case of unexpected exception rollback to previous state, log and raise
    # the error again.
    except Exception as e:
        log.error('Error when trying to add new weapon. {e} | {q}'
                  .format(e=e, q=query))

        db.rollback()
        log.error('Rollback to previous state.')
        raise e
    finally:
        # whatever happend close the dabase and return <completed> that should
        # be true if posting succeded or false in case of errors
        cursor.close()
        db.close()
        log.info('POST COMPLETED in %.4fs' % (time.time() - init_time))
        return completed


# get a list of all the values for the given key in the given table
def getList(tableName, key='name'):
    """
    Get all the values in  ``key`` column of the given table.
    Useful to generate lists of all the available items to select from the
    given table.

    :param str tableName: Name of the table to query
    :param str key: Table column to query
    :default key: `name`

    :returns: ``tuple`` of strings

    :raises: * ``IndexError`` if table or column do not exist.
             * ``ValueError`` for caught sql errors

    """
    # open database or raise OSError if file not found
    db = connect()
    # validate the table name or raise IndexError if table is not found
    table_name = _validateTableName(db, tableName)

    key = key.lower()
    if key not in _getHeaders(db.cursor(), tableName):
        raise IndexError('Requested column "%s" does not exist in "%s" table'
                         % (key, table_name))

    # This separator will be used to split the values retrieved from the query
    # for serialization
    separator = '|'

    query = "SELECT GROUP_CONCAT(%s, '%s') FROM %s;"
    query = query % (key, separator, table_name)

    # string containing all the items in the requested column separated the
    # specified separator (a | )
    try:
        items = str(db.execute(query).fetchall()[0][0])
    except Exception, error:
        m = 'Error in fetching values from {t}:{c} in {d}'
        m = m.format(t=table_name, c=key, d=DATABASE)
        log.error(m)
        raise ValueError(error)
    finally:
        db.close()

    # generate the list of all the items. Will be returned as tuple
    data = []
    for item in items.split('|'):
        data.append(item)

    return tuple(data)


if __name__ == '__main__':
    """
    Test the database connection if run as main
    """
    log.setLevel(logging.INFO)
    _test()
    get('weapon', 'svasti')
