# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './qt/home.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Homepage(object):
    def setupUi(self, Homepage):
        Homepage.setObjectName(_fromUtf8("Homepage"))
        Homepage.resize(545, 451)
        self.gridLayout = QtGui.QGridLayout(Homepage)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        spacerItem = QtGui.QSpacerItem(20, 52, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem, 0, 1, 1, 1)
        self.label_AppTitle = QtGui.QLabel(Homepage)
        font = QtGui.QFont()
        font.setPointSize(36)
        font.setBold(True)
        font.setWeight(75)
        self.label_AppTitle.setFont(font)
        self.label_AppTitle.setScaledContents(False)
        self.label_AppTitle.setAlignment(QtCore.Qt.AlignCenter)
        self.label_AppTitle.setObjectName(_fromUtf8("label_AppTitle"))
        self.gridLayout.addWidget(self.label_AppTitle, 1, 1, 1, 1)
        self.pushButton_goTo_game = QtGui.QPushButton(Homepage)
        self.pushButton_goTo_game.setEnabled(False)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8(":/fA/fort-awesome.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_goTo_game.setIcon(icon)
        self.pushButton_goTo_game.setObjectName(_fromUtf8("pushButton_goTo_game"))
        self.gridLayout.addWidget(self.pushButton_goTo_game, 2, 1, 1, 1)
        spacerItem1 = QtGui.QSpacerItem(3, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem1, 3, 0, 1, 1)
        self.pushButton_goTo_armory = QtGui.QPushButton(Homepage)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(_fromUtf8(":/fA/industry.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_goTo_armory.setIcon(icon1)
        self.pushButton_goTo_armory.setObjectName(_fromUtf8("pushButton_goTo_armory"))
        self.gridLayout.addWidget(self.pushButton_goTo_armory, 3, 1, 1, 1)
        spacerItem2 = QtGui.QSpacerItem(4, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem2, 3, 2, 1, 1)
        self.pushButton_goTo_characters = QtGui.QPushButton(Homepage)
        self.pushButton_goTo_characters.setEnabled(False)
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(_fromUtf8(":/fA/users.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_goTo_characters.setIcon(icon2)
        self.pushButton_goTo_characters.setObjectName(_fromUtf8("pushButton_goTo_characters"))
        self.gridLayout.addWidget(self.pushButton_goTo_characters, 4, 1, 1, 1)
        spacerItem3 = QtGui.QSpacerItem(20, 53, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.MinimumExpanding)
        self.gridLayout.addItem(spacerItem3, 5, 1, 1, 1)
        self.pushButton_goTo_settings = QtGui.QPushButton(Homepage)
        self.pushButton_goTo_settings.setEnabled(False)
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(_fromUtf8(":/fA/cogs.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_goTo_settings.setIcon(icon3)
        self.pushButton_goTo_settings.setObjectName(_fromUtf8("pushButton_goTo_settings"))
        self.gridLayout.addWidget(self.pushButton_goTo_settings, 6, 1, 1, 1)
        self.pushButton_quit = QtGui.QPushButton(Homepage)
        icon4 = QtGui.QIcon()
        icon4.addPixmap(QtGui.QPixmap(_fromUtf8(":/fA/power-off.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_quit.setIcon(icon4)
        self.pushButton_quit.setObjectName(_fromUtf8("pushButton_quit"))
        self.gridLayout.addWidget(self.pushButton_quit, 7, 1, 1, 1)
        spacerItem4 = QtGui.QSpacerItem(20, 52, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem4, 8, 1, 1, 1)

        self.retranslateUi(Homepage)
        QtCore.QMetaObject.connectSlotsByName(Homepage)

    def retranslateUi(self, Homepage):
        Homepage.setWindowTitle(_translate("Homepage", "Homepage", None))
        self.label_AppTitle.setText(_translate("Homepage", "PyD20 Admin control", None))
        self.pushButton_goTo_game.setText(_translate("Homepage", "&Game", None))
        self.pushButton_goTo_armory.setText(_translate("Homepage", "&Armory", None))
        self.pushButton_goTo_characters.setText(_translate("Homepage", "&Characters", None))
        self.pushButton_goTo_settings.setText(_translate("Homepage", "&Settings", None))
        self.pushButton_quit.setText(_translate("Homepage", "&Quit", None))

import icons_rc
