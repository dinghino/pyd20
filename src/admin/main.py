from PyQt4 import QtGui
from PyQt4.QtCore import Qt, QString, SIGNAL, QThread
import sys
from os.path import abspath
import re
import time
import copy
import logging
import mainui
import home
import armory

if __name__ == '__main__':
    sys.path.insert(0, abspath('.'))

import context
from game.components import constants
import database

try:
    _fromUtf8 = QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s


log = logging.getLogger(__name__)


class GetWeaponsFromSQL(QThread):

    done = SIGNAL('weapons_downloaded(PyQt_PyObject)')

    def __init__(self, parent=None):
        super(GetWeaponsFromSQL, self).__init__(parent)

    def run(self):
        """Fetch data. """
        log.info('Fetching weapons data...')
        data = database.get('weapon')
        log.info('Data retrieved')
        self.emit(self.done, data)


class SaveWeaponToSQL(QThread):

    get_data = SIGNAL('save_data_to_sql(PyQt_PyObject)')
    done = SIGNAL('weapons_saved')

    def __init__(self, parent=None):
        super(SaveWeaponToSQL, self).__init__(parent)
        self.connect(self, self.get_data, self.upload_data)

    def upload_data(self, data):
        """
        Store locally the data and start the thread to save data into sql.
        """
        self.data = data
        self.start()

    def run(self):
        """Save data to the sql database """
        now = time.time()
        log.info('Uploading data...')
        database.post('weapon', self.data)
        log.info('Upload complete in ~%.3fs' % (time.time() - now))
        self.emit(self.done)


class HomePage(QtGui.QWidget, home.Ui_Homepage):
    def __init__(self, parent=None):
        super(HomePage, self).__init__(parent)
        self.setupUi(self)


class AppArmoryWindow(QtGui.QMainWindow, armory.Ui_ArmoryWindow):

    # Constants used to generate combo boxes inside the form to handle weapon
    # creation and modification
    formData = {
        'weaponFamily': ['Simple', 'Martial', 'Exotic'],
        'weaponType': ['Unarmed', 'Light', 'One-handed',
                       'Two-handed', 'Ranged'],
        'damageType': ['', 'Piercing', 'Bludgeoning', 'Slashing'],
        'damage_xor': ['', 'and', 'or'],
        'coinType': ['CP', 'SP', 'GP', 'PP']
        }

    def __init__(self, parent=None):
        super(AppArmoryWindow, self).__init__(parent)
        self.setupUi(self)

        # List that will contain all the items retrieved from the sqlite
        # database as dicts describing weapons
        self._armory_data = []

        self._itemsQty = 0   # weapons counted -> table rows
        self._keysCount = 0  # unique keys in weapons -> table columns

        # Texts for the table's header. Will be filled with all the keys of the
        # objects
        self._tableHeaders = []
        self._viewedWeapon = None      # selected weapon in QList in Edit tab
        self._viewedWeapon_idx = None  # idx of the viewed Weapon in list

        # Create Thread to fetch data
        self.get_thread = GetWeaponsFromSQL(self)
        self.post_thread = SaveWeaponToSQL(self)

        # setup form content
        self.setupForm()

        # setup connections between GUI elements and classes
        self.setupConnections()

        # Fetch data as soon as the window is created
        self.get_thread.start()

    def getWeaponTemplate(self):
        """
        Return the weapon template to create new weapons or reset the form.
        """
        try:
            objId = len(self._armory_data)
        except:
            log.error('Cannot get correct id for weapon template')
            objId = None

        uuid = database.generate_uuid()

        weaponTemplate = {
            "id": objId,
            "name": "New weapon",
            "weight": 0.0,
            "cost": "0 GP",
            "damage": "1d4 x2",
            "description": "",
            "weapon family": "Simple",
            "weapon type": "Light",
            "damage type": "Slashing",
            "weight": 0,
            "increment": 0,
            "uuid": uuid
            }

        return weaponTemplate

    def setupContent(self):
        """Evaluate the content of the data structure, setup table and list
        structure and content.
        """

        self._generateHeaders()

        # generate or update the content inside the TableWidget
        self._generateTableContent()
        self._generateListContent()

    def setupForm(self):
        """Setup form elements such as combo boxes or other components. """

        data = self.formData

        for i in data['weaponFamily']:
            self.combo_weaponFamily.addItem(i)

        for i in data['weaponType']:
            self.combo_weaponType.addItem(i)

        for i in data['damageType']:
            self.combo_damageType_1.addItem(i)
            self.combo_damageType_2.addItem(i)

        # remove first (blank) item from damageType_1 since at least 1 type
        # must exist
        self.combo_damageType_1.removeItem(0)
        # remove first item (after blank line) from damageType_2 since it's
        # already selected in damageType_1
        self.combo_damageType_2.removeItem(1)

        for i in data['damage_xor']:
            self.combo_damageType_xor.addItem(i)

        for i in data['coinType']:
            self.combo_price_coinType.addItem(i)

        # set hidden fields by default
        self.spinBox_dmg_sec_faces.setHidden(True)
        self.spinBox_dmg_sec_dices.setHidden(True)
        self.spinBox_dmg_sec_bonus.setHidden(True)
        self.label_dmg_sec.setHidden(True)

    def setupConnections(self):
        """Signals between components. """

        # update window content when data has been loaded.
        self.connect(self.get_thread, self.get_thread.done, self._updateWindow)

        # connect the save to sql thread signal for save done.
        self.connect(self.post_thread,
                     self.post_thread.done,
                     self._sqlSaveDone)

        # set the currently selected item in the list for view
        self.listWidget.currentItemChanged.connect(self._setViewedWeapon)

        # udate local _armory_data with the form content
        self.pushButton_save.clicked.connect(self.updateLocalArmory)

        # reset the form with the initial data for the item
        self.pushButton_reset.clicked.connect(self._resetChangesToArmory)

        # start adding a new item.
        self.pushButton_addNew.clicked.connect(self.addNewWeapon)

        # set (or clear) the xor combo box depending on what is selected in
        # type 2
        dmgCombo_2 = self.combo_damageType_2
        dmgCombo_2.currentIndexChanged.connect(self._force_xor_comboBox)
        # remove the item selected in the first damage type from the second one
        dmgCombo_1 = self.combo_damageType_1
        dmgCombo_1.currentIndexChanged.connect(self._handle_dmgType_change)

        # filter out the list with the 'search' line edit
        self.lineEdit_filterList.textChanged.connect(self._filterList)

    def addNewWeapon(self):
        """Add a new weapon to _armory_data, to the list and select that one
        for editing. """

        armory = self._armory_data
        template = self.getWeaponTemplate()

        # regex to check if there are any new weapons already stored
        reStr = r'^{0}(?:\s\d+)?$'.format(template['name'])

        # add template at the beginning of armory data
        newWeapon = copy.copy(template)

        # find other weapons with the same name, to avoid duplicates
        otherNewWeapons = [i for i in armory if re.match(reStr, i['name'])]

        if len(otherNewWeapons) > 0:
            newWeapon['name'] += ' %s' % len(otherNewWeapons)

        # add the new object to the local armory list
        self._armory_data.append(newWeapon)

        # set as viewed weapon
        idx = len(self._armory_data) - 1
        self._viewedWeapon = self._armory_data[idx]
        self._viewedWeapon_idx = idx

        # create the new item list
        newItem = QtGui.QListWidgetItem(self._viewedWeapon['name'])
        color = QtGui.QColor(200, 200, 0, 255)
        newItem.setTextColor(color)
        # add the new item list, select it and sort the list
        self.listWidget.addItem(newItem)
        self.listWidget.setCurrentItem(newItem)
        self.listWidget.sortItems()

        self.tabWidget_views.setCurrentIndex(1)

        # focus and clear the lineEdit for the name of the new weapon
        self.lineEdit_name.setFocus()
        self.lineEdit_name.setText('')

    def updateLocalArmory(self):
        """Store the changes made into the form (updated in self._viewedWeapon)
        back into _armory_data, ready to be saved. """
        # update content of the viewed weapon with what is inside the form at
        # this point.
        self._loadWeaponInForm()
        # set the name in the list to a BOLD fond, so we know what we have
        # changed (changes are still permanent in the session)
        idx = self._viewedWeapon_idx
        self._armory_data[idx].update(self._viewedWeapon)

        # add the new weapon inside the table view
        self._generateTableContent()

        # update the list with the changes
        color = QtGui.QColor(200, 0, 0, 255)
        listItem = self.listWidget.currentItem()
        listItem.setText(QString(self._viewedWeapon['name']))
        listItem.setTextColor(color)

        self.listWidget.sortItems()

    def saveSQL_weapon(self):
        """
        Notify the POST thread that we want to save the data and pass it the
        armory data through a signal.

        When the signal is received from the thread it will store locally the
        data and call its ``start`` method to properly initialize the thread.
        """
        self.post_thread.emit(self.post_thread.get_data, self._armory_data)

    def _generateHeaders(self):
        """
        Get the headers from the database and setup _tableHeaders and
        _keysCount properties to correctly handle ui creation.
        """

        db = database.connect()
        c = db.cursor()
        self._tableHeaders = tuple(database._getHeaders(c, 'weapon'))
        self._keysCount = len(self._tableHeaders)
        c.close()
        db.close()

    @property
    def form_weaponDamage(self):
        """All the parts of the weapon damage formatted as a single string. """

        def compileDice(rolls, faces, bonus):
            string = '%sd%s' % (rolls, faces)  # dice to roll (no bonus)
            if bonus is not 0:
                # if bonus is not 0 add that too, adding a + sign if > 0
                string += '%s' % ('+%s' % bonus if bonus > 0 else bonus)
            return string

        dmgString = ''

        d1_r = self.spinBox_dmg_prim_dices.value()
        d1_f = self.spinBox_dmg_prim_faces.value()
        d1_b = self.spinBox_dmg_prim_bonus.value()
        # extra damage dice
        d2_r = self.spinBox_dmg_sec_dices.value()
        d2_f = self.spinBox_dmg_sec_faces.value()
        d2_b = self.spinBox_dmg_sec_bonus.value()

        dmgString += compileDice(d1_r, d1_f, d1_b)
        # if the secondary damage is checked, add that to the string
        if self.checkBox_dmgSecondary.isChecked():
            dmgString += '/%s' % compileDice(d2_r, d2_f, d2_b)

        # add the threat if lower than 20
        if self.spinBox_critThreat.value() < 20:
            dmgString += ' %s-20' % (self.spinBox_critThreat.value())

        # add the multiplier
        dmgString += ' x%s' % self.spinBox_critMultiplier.value()

        # damage string =>
        # <rolls>d<faces>(<bonus>)(/<rolls>d<faces>(<bonus>)) <th>-20 x<multi>
        # as in 1d6+1/1d8+1 19-20 x2 | 1d4 20-20 x2 | 1d8-1 20-20 x2
        valid = re.match(constants._WEAPON_DMG_REGEX, dmgString)
        if not valid:
            raise ValueError('There has been an issue with the weapon damage')

        del valid
        return dmgString

    @form_weaponDamage.setter
    def form_weaponDamage(self, dmgString):

        # split string values into usable strings
        dmg_regex = constants._WEAPON_DMG_REGEX
        dice_regex = constants._DICE_REGEX

        # get the match
        weaponDmg = re.match(dmg_regex, dmgString)
        if not weaponDmg:
            raise ValueError('Weapon damage provided for the form is'
                             'not in a valid form: %s' % dmgString)

        dmg_dice1 = weaponDmg.group(1)
        dmg_dice2 = weaponDmg.group(2)
        threat = weaponDmg.group(3) or '20'
        multiplier = weaponDmg.group(4) or '2'

        # set simple values: crit threat and multiplier
        self.spinBox_critThreat.setValue(int(threat))
        self.spinBox_critMultiplier.setValue(int(multiplier))

        # set damage values
        dmg_dice1_value = re.match(dice_regex, dmg_dice1)

        dice1_dices = dmg_dice1_value.group(1)
        dice1_faces = dmg_dice1_value.group(2)
        bonus1 = dmg_dice1_value.group(3) or '0'

        # secondary damage values
        self.spinBox_dmg_prim_dices.setValue(int(dice1_dices))
        self.spinBox_dmg_prim_faces.setValue(int(dice1_faces))
        self.spinBox_dmg_prim_bonus.setValue(int(bonus1))

        try:  # try secondary damage if dice matches
            dmg_dice2_value = re.match(dice_regex, dmg_dice2)

            dice2_dices = dmg_dice2_value.group(1) or 0
            dice2_faces = dmg_dice2_value.group(2) or 0
            bonus2 = dmg_dice2_value.group(3) or '0'

            self.spinBox_dmg_sec_dices.setValue(int(dice2_dices))
            self.spinBox_dmg_sec_faces.setValue(int(dice2_faces))
            self.spinBox_dmg_sec_bonus.setValue(int(bonus2))
            # This is at the end so if we have exceptions
            # (dice don't exists) this won't be triggered and the section
            # will remain hidden
            self.checkBox_dmgSecondary.setChecked(True)
        except:
            self.checkBox_dmgSecondary.setChecked(False)

    @property
    def form_weaponName(self):
        return str(self.lineEdit_name.text())

    @form_weaponName.setter
    def form_weaponName(self, string):
        self.lineEdit_name.setText(QString(string))

    @property
    def form_weaponDamageType(self):
        # damage type compiling => '<Type> (<and/or> <Type>)'
        damageType = str(self.combo_damageType_1.currentText())

        # if we have a secondary damage type add that and the xor to the string
        if self.combo_damageType_2.currentIndex() is not 0:
            xor = str(self.combo_damageType_xor.currentText())
            secondType = str(self.combo_damageType_2.currentText())
            damageType += ' %s %s' % (xor, secondType)

        return damageType

    @form_weaponDamageType.setter
    def form_weaponDamageType(self, string):
        dmg_types = string.split(' ')

        self._setComboBoxValue('combo_damageType_1', dmg_types[0])

        try:
            self._setComboBoxValue('combo_damageType_xor', dmg_types[1])
            self._setComboBoxValue('combo_damageType_2', dmg_types[2])
        except:
            self._setComboBoxValue('combo_damageType_xor', '')
            self._setComboBoxValue('combo_damageType_2', '')

    @property
    def form_weaponFamily(self):
        return str(self.combo_weaponFamily.currentText())

    @form_weaponFamily.setter
    def form_weaponFamily(self, string):
        self._setComboBoxValue('combo_weaponFamily', string)

    @property
    def form_weaponType(self):
        return str(self.combo_weaponType.currentText())

    @form_weaponType.setter
    def form_weaponType(self, string):
        self._setComboBoxValue('combo_weaponType', string)

    @property
    def form_weaponWeight(self):
        return float(self.lineEdit_weight.text())

    @form_weaponWeight.setter
    def form_weaponWeight(self, value):
        self.lineEdit_weight.setText(QString(str(value)))

    @property
    def form_rangeIncrement(self):
        return int(self.lineEdit_increment.text())

    @form_rangeIncrement.setter
    def form_rangeIncrement(self, value):
        self.lineEdit_increment.setText(QString(str(value)))

    @property
    def form_weaponCost(self):
        coins = self.lineEdit_price_coin.text()
        coinType = str(self.combo_price_coinType.currentText())

        return '%s %s' % (coins, coinType)

    @form_weaponCost.setter
    def form_weaponCost(self, string):
        # split the string
        valid = re.match(constants._MONEY_REGEX, string)
        if not valid:
            raise ValueError('Passed weapon cost is not a valid format. '
                             'It should be "<value> <type>"')

        price = tuple(string.split(' '))

        self.lineEdit_price_coin.setText(QString(price[0]))

        try:
            self._setComboBoxValue('combo_price_coinType', price[1])
        except:
            self._setComboBoxValue('combo_price_coinType', 'CP')

    @property
    def form_weaponText(self):
        return str(self.textEdit_description.toPlainText())

    @form_weaponText.setter
    def form_weaponText(self, string):
        self.textEdit_description.setText(QString(string))

    def _updateWindow(self, listData):
        """Callback for GetWeaponsFromSQL run method. called when data has been
        loaded from json file. This will trigger some other methods in chain,
        regenerating all window's contents. """
        self.lineEdit_filterList.setText('')
        # store the new data
        self._armory_data = listData

        # validate currently viewed weapon (if any) index.
        # This is to avoid index error when reloading the database data after
        # adding a new item locally without posting it on the database and
        # having it selected when reloading the data, causing for example
        # to have 50 items (max id 49), adding the 51 (idx 50) and trying to
        # get the idx 50 that does not exist.
        _idx = self._viewedWeapon_idx
        self._viewedWeapon_idx = _idx if _idx < len(self._armory_data) else 0

        log.info('Data ready: %s weapons present.' % len(listData))

        # Setup the content for all the elements in the widget
        self.setupContent()

    def _sqlSaveDone(self):
        log.info('data saved')
        self._generateTableContent()
        self._generateListContent()

    def _generateTableContent(self):
        table = self.tableWidget

        # generate the table rows and columns to show current weapons.json data
        table.setRowCount(len(self._armory_data))
        table.setColumnCount(len(self._tableHeaders))
        table.setHorizontalHeaderLabels(self._tableHeaders)

        # add the content to the table cells
        for row in range(table.rowCount()):
            for column in range(table.columnCount()):

                # key to get the data from the parsed json
                key = self._tableHeaders[column]

                # get the cell content
                try:
                    string = str(self._armory_data[row][key])
                except KeyError:
                    string = ''

                # create the QTable Item from the json
                item = QtGui.QTableWidgetItem(string)

                # Add the item in the correct cell
                table.setItem(row, column, item)

        # force stretch full window width for TableWidget
        table.horizontalHeader().setResizeMode(QtGui.QHeaderView.Stretch)

    def _generateListContent(self, items=None):
        self.listWidget.clear()
        if not items:
            items = self._armory_data

        for item in items:
            listItem = QtGui.QListWidgetItem(item['name'])
            self.listWidget.addItem(listItem)

        self.listWidget.sortItems()

    def _resetChangesToArmory(self):
        log.info('Resetting changes')
        listItem = self.listWidget.currentItem()
        if listItem:
            color = QtGui.QColor(0, 0, 0, 255)
            listItem.setTextColor(color)

        self._setViewedWeapon()

    def _loadWeaponInForm(self):
        """Save current form status in _viewedWeapon, ready to be saved in the
        armory and then in the JSON file. """
        weapon = self._viewedWeapon

        # rewrite the weapon attributes
        weapon['name'] = self.form_weaponName
        weapon['weapon family'] = self.form_weaponFamily
        weapon['weapon type'] = self.form_weaponType
        weapon['cost'] = self.form_weaponCost
        weapon['weight'] = self.form_weaponWeight
        weapon['damage'] = self.form_weaponDamage
        weapon['damage type'] = self.form_weaponDamageType
        weapon['description'] = self.form_weaponText
        weapon['increment'] = self.form_rangeIncrement

    def _setViewedWeapon(self, weaponName=None):
        """Find the dictionary for the weapon with the given name and store a
        link to the viewed weapon dictionary and its index into local variables
        to view and edit its content.

        If no name is passed it will reload the current weapon from the data,
        resetting everything.
        """

        if weaponName:
            for i, weapon in enumerate(self._armory_data):
                if weapon['name'] == weaponName.text():
                    # self._viewedWeapon = weapon
                    self._viewedWeapon_idx = i
                    break

        try:
            self._viewedWeapon = self._armory_data[self._viewedWeapon_idx]
        except IndexError:
            m = 'Index error: {v} {m}'.format(m=len(self._viewedWeapon) - 1,
                                              v=self._viewedWeapon_idx)
            log.error(m)

            self._viewedWeapon = self._armory_data[0]
            self._viewedWeapon_idx = 0

        self._viewWeaponInForm(self._viewedWeapon)
        self.pushButton_save.setEnabled(True)

    def _setComboBoxValue(self, name, value=None, defIdx=0):
        """Find the component by name, search the index of the passed value
        and, if it exists(>= 0), set it as current.
        You can also force and index passing the def property."""
        comboBox = getattr(self, name)
        try:
            idx = comboBox.findText(value, Qt.MatchFixedString)
        except TypeError:
            idx = defIdx
        if idx < 0:
            idx = defIdx
        comboBox.setCurrentIndex(idx)

    def _resetForm(self):
        """Reset the form value to its default values.. """
        template = self.getWeaponTemplate()

        self.form_weaponName = template['name']
        self.form_weaponType = template['weapon type']
        self.form_weaponDamage = template['damage']
        self.form_weaponDamageType = template['damage type']
        self.form_weaponCost = template['cost']
        self.form_weaponWeight = template['weight']
        self.form_weaponText = template['description']
        self.form_rangeIncrement = template['increment']

    def _viewWeaponInForm(self, weapon):
        """Update the form with the content of self._viewedWeapon dictionary.
        """
        self._resetForm()

        self.form_weaponName = weapon['name']
        self.form_weaponFamily = weapon['weapon family']
        self.form_weaponType = weapon['weapon type']
        self.form_weaponDamage = weapon['damage']
        self.form_weaponDamageType = weapon['damage type']
        self.form_weaponCost = weapon['cost']
        self.form_weaponWeight = weapon['weight']
        self.form_weaponText = weapon['description']
        self.form_rangeIncrement = weapon['increment']
        # self.label_uuid.setText(QString(weapon['uuid']) or QString(''))

    def _force_xor_comboBox(self, dmgType_2_idx=None, value=None):
        """Force 'AND' in the damage type xor combo box. Callback for second
        damage type combobox signal, so we are sure to have the xor operator
        available.

        If a <value> is passed the box will be forced on that one.
        """
        combobox = self.combo_damageType_xor
        idx = dmgType_2_idx

        if value:
            # if a value has been passed, search for that and set it as active.
            # If valus is not a string it may cause a ValueError.
            try:
                combobox.setCurrentIndex(combobox.findText(str(value)))
            except:
                pass

        elif idx == 0:
            # else (no value), if the passed index is 0 (current dmgType_2 is
            # blank) force the xor to blank
            combobox.setCurrentIndex(0)

        elif combobox.currentIndex() == 0 and idx != 0:
            # else (idx != 0), if current xor is blank force 1
            combobox.setCurrentIndex(1)

        # else, leave as it is.

    def _handle_dmgType_change(self, index):
        """Hide the selected primary damage type from the combobox of the
        second type to avoid double selection.

        Indexes are different as the second combo box has 1 more item ("")
        at the beginning, so combo_1[0] == combo_2[1] and so on.
        """

        combo_1 = self.combo_damageType_1
        combo_2 = self.combo_damageType_2
        # current value to reset stuff where they should be
        current_1 = str(combo_1.itemText(index))
        current_2 = str(combo_2.currentText())

        # regenerate the combo box excluding the selected value of combo_1
        combo_2.clear()
        for item in self.formData['damageType']:
            if item == current_1:
                continue

            combo_2.addItem(item)

        # reset previously selected value if still present
        current_2_pos = combo_2.findText(current_2)
        combo_2.setCurrentIndex(current_2_pos)

        # reset the value for the xor combobox
        # setting idx = 0 if combo_2 idx = 0
        if current_2_pos == -1:
            self._force_xor_comboBox(0)

    def _filterList(self, text):
        """Filter out the list of items in edit view with the given text. """
        # list that will be filled with all matching objects
        filteredItems = []

        for item in self._armory_data:
            text = str(text).lower()
            name = item['name'].lower()
            wType = item['weapon type'].lower()
            wFamily = item['weapon family'].lower()
            # if the passed text exists in any of the previous keys, add the
            # item to the list
            if text in name or text in wType or text in wFamily:
                filteredItems.append(item)

        # regenerate the list widget
        if len(filteredItems) > 0:
            self._generateListContent(filteredItems)
        else:
            self.listWidget.clear()


class MainAppWindow(QtGui.QMainWindow, mainui.Ui_MainWindow):
    def __init__(self, parent=None):
        super(MainAppWindow, self).__init__(parent)
        self.setupUi(self)

        # widgets
        self.homepage = HomePage(self)
        self.armoryWindow = AppArmoryWindow(self)

        # widget wrapper
        self.widgets = QtGui.QStackedWidget(self)
        self.widgets.addWidget(self.homepage)
        self.widgets.addWidget(self.armoryWindow)

        self.armoryWindow.hide()

        self.setupConnections()
        # set initial widget
        self.setCentralWidget(self.widgets)
        self.goToHomepage()

    def goToArmory(self):
        """Go to the Armory handler widget."""
        sizeHint = self.armoryWindow.minimumSize()
        self.setMinimumSize(sizeHint)
        self.widgets.setCurrentWidget(self.armoryWindow)
        self.toolBar_armory.show()

    def goToHomepage(self):
        """Go back to the homepage. """
        self.widgets.setCurrentWidget(self.homepage)
        self.toolBar_armory.hide()

    def setupConnections(self):
        """Setup connection between application elements, buttons, widgets,
        windows... """
        # setup menu voices connections
        self._setupMenu()
        self._setupConnections_homepage()
        self._setupConnections_armory_toolbar()

    def _setupConnections_homepage(self):
        """Setup connections for the home page. """
        self.homepage.pushButton_quit.clicked.connect(self._quitApp)
        self.homepage.pushButton_goTo_armory.clicked.connect(self.goToArmory)

    def _setupConnections_armory_toolbar(self):

        self.connect(self.actionBackHome,
                     SIGNAL('activated()'),
                     self.goToHomepage)

        # save armory from MainWindow toolBar
        self.connect(self.actionSaveWeapons,
                     SIGNAL('activated()'),
                     self.armoryWindow.saveSQL_weapon)

        # reset the armory content with the JSON (reload the file)
        self.connect(self.actionReloadWeapons,
                     SIGNAL('activated()'),
                     self.armoryWindow.get_thread.start)

        # start adding a new weapon.
        self.connect(self.actionAddWeapon,
                     SIGNAL('activated()'),
                     self.armoryWindow.addNewWeapon)

    def _setupMenu(self):
        self.menu_view_home.activated.connect(self.goToHomepage)
        self.menu_view_armory.activated.connect(self.goToArmory)
        self.menu_file_quit.activated.connect(self._quitApp)
        # TODO: complete connections

    def _quitApp(self):
        """ Close all the windows of the application, terminating the
        execution. Should also interrupt (or let finish) other process in
        different threads. """
        self.armoryWindow.close()
        self.close()


def run():
    app = QtGui.QApplication(sys.argv)
    mainWindow = MainAppWindow()
    mainWindow.show()
    app.exec_()

if __name__ == '__main__':
    run()
