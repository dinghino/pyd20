""" GameClass generator for characters. """
if __name__ == '__main__':
    import sys, os.path
    sys.path.insert(0, os.path.abspath('.'))
    import context

import logging
from src import database

log = logging.getLogger(__name__)


class GameClass(object):
    """Game Classes are generated automatically from classes.json using the
    given name and linked to the owner to pass data to the Character object.

    if the requested name argument is not found, fallback is 'Fighter'.

    :param str name: the name of the GameClass to used

    """

    __defClass = 'fighter'

    def __init__(self, className):
        """Constructor. """

        self.__classData = self._fetch_class_sql(className)
        # This suppose a character level of 1. Will be updated when the
        # character is actually created and every time it levels up
        self._level = 1

        self._setInitialData()

    @property
    def owner(self):
        """ Character instance owner of this GameClass. """
        return self._owner

    @property
    def name(self):
        """ Name of the GameClass. """
        return self._name

    @property
    def hitDice(self):
        """ Dice to roll for health points. """
        return self._hitDice

    @property
    def bonus(self):
        """
        Bonuses given by the class at the current level.
        Returns a ``dict`` that includes:

        * Base attack bonus (``baseAttackBonus``)
        * Fortitude saving throw (``fortitude``)
        * Reflex saving throw (``reflex``)
        * Will saving throw (``will``)

        """
        return self._bonus

    @property
    def primaryStat(self):
        """ Primary ability for this class. """
        return self._keyStat

    @property
    def secondaryStat(self):
        """ Secondary ability for the class. """
        return self._secStat

    @property
    def proficiencies(self):
        """ Returns the weapon proficiencies ``list`` for the class. """
        return self._proficiencies

    def _fetch_class_sql(self, className):
        """Extract all the data of the GameClass from classes.json. """

        class_name = database._generate_slug(className)
        gameClass = database.get('classes', class_name)

        if not gameClass:
            # If the get returned zero items, notify with the logger and
            # try to get the default class set in the GameClass class.
            m = "class '{cls}' not found in database. defaulting to {default}."
            m = m.format(cls=class_name, default=self.__defClass)
            log.warn(m)
            class_name = self.__defClass
            gameClass = database.get('classes', class_name)

        # Last check to see if a game class has been fetched from the database
        if not gameClass:
            m = 'Error while creating a game class object: {n}'
            m = m.format(n=class_name)
            log.error(m)
            raise Exception(m)

        return gameClass

    def _setInitialData(self):
        data = self.__classData

        self._name = data['name']
        self._slug = data['slug']
        self._hitDice = data['hit_dice']
        self._keyStat = data['key_stat']
        self._secStat = data['secondary_stat']
        self._isCaster = data['spell_caster']
        self._startingMoney = data['starting_money']
        self._proficiencies = data['proficiencies']

        # this dictionary will be updated by _updateClassBonus, called on init
        # and whenever the character levels up
        self._bonus = {}

    def levelUp(self, level):
        """
        Change the game class level and update all the relative bonuses
        automatically
        """
        self._level += level
        self._updateClassBonus()

    def _updateClassBonus(self):
        """Update the basic bonuses for the class.

        Updates the base attack bonus and saving throws using the current
        level of the class, fetching the data from the sqlite class_progression
        of this game class.

        This is automatically called whenever the gameClass levels up.
        """
        progress_table = 'class_progression_' +  self._slug
        data = database.get(progress_table, value=self._level, key='level')

        self._bonus['baseAttackBonus'] = data['base_attack_bonus']

        # Saving throws
        self._bonus['fortitude'] = data['fortitude']
        self._bonus['reflex'] = data['ref']
        self._bonus['will'] = data['will']


if __name__ == '__main__':

    from game import Character
    c = Character(race='dwarf', str=18, con=18)

    print c, '| AC:', c.armorClass, '\n', c.abilities, '\nBAB:', c.baseAttackBonus,\
          '| MAB:', c.meleeAttackBonus, '| RAB:', c.rangedAttackBonus

    c.levelUp(9)
    print c, '| AC:', c.armorClass, '\n', c.abilities, '\nBAB:', c.baseAttackBonus,\
          '| MAB:', c.meleeAttackBonus, '| RAB:', c.rangedAttackBonus
