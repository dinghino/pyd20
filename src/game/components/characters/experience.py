"""
Experience class is a base utility class that is used to handle characters'
experience, and it will be responsible for triggering the actual
level up of a character (and of all the other related utility classes) when
needed.

Experience needed for the next (or previous) levels are calculated internally
so there is actually no limit, at the moment, for the maximum level of a
character, even though it will be limited to level 20 in the future due to
limits in the gameClasses and Races bonuses for higher levels.
"""

import logging

log = logging.getLogger(__name__)


class Experience(object):
    """
    Experience objects are used to handle the experience of the characters,
    allowing them to gain - and lose - experience and in the end level up.

    Each Instance starts at level 1 (as do all the other modules related
    the the characters) and receives an ``owner`` parameter that is supposed to
    be an instance a *primary class* (Character) that can ``levelUp()``.
    """

    def __init__(self, owner=None):
        self._level = 1
        self._owner = owner

        # current experience value
        self._current = 1

        # exp for the next level
        self._exp_to_level_up = self._calc_exp_to_next(self._level)

        # exp for the previous level. using when levelling down.
        self._exp_to_current = 0

    @property
    def level(self):
        """
        Current level value.

        :returns: ``int``
        """
        return self._level

    @property
    def owner(self):
        """ Instance of the owner class. """
        return self._owner

    @property
    def current(self):
        """
        Current total experience value.

        :returns: ``int``
        """
        return self._current

    @property
    def to_next_level(self):
        """
        Total experience required for the next level up.

        :returns: ``int``
        """
        return self._exp_to_level_up

    @property
    def percentage(self):
        """
        Prcentage of the current level experience.

        :returns: ``int`` between 0 and 100
        """
        current_level_exp = self._exp_to_level_up - self._exp_to_current
        current_exp_on_level = self.current - self._exp_to_current

        # floating percentage value
        perc = float(current_exp_on_level) / float(current_level_exp)

        # return the integer between 0 - 100
        return int(perc * 100)

    def gain(self, exp):
        """
        Gain some experience and levelup if necessary.

        :param int exp: experience gained

        """
        self._current += exp

        if self._current >= self._exp_to_level_up:
            # Check if the current experience has reached the requirement for
            # the next level. if so level up...
            while (self._current > self._exp_to_level_up):
                # and continue doing so until is over. This allows gaining more
                # than one level at a time.
                #
                # NOTE: This goes against 3.5 rules that would want to gain 1
                #       level at a time and if the given exp is enough to go up
                #       two (or more levels) we should just reach the next one
                #       - 1 xp.
                #
                #       example: level 1, gets 3500 experience.
                #                required for level 2 is 2000
                #                required for level 3 is 3000
                #                character should reach level 2 and 2999 exp
                #
                #      This feature will be added in the future directly into
                #      _levelUp() method
                self._levelUp(1)

    def lose(self, exp):
        """
        Lose some experience and level down if necessary.

        :param int exp: experience lost

        """
        self._current -= exp

        if self._current <= self._exp_to_current:
            # check if we lost enough experience to go down one level
            while (self._current < self._exp_to_current):
                # and keep going down until we don't have enough to lose levels
                self._levelUp(-1)

    def _levelUp(self, levels=1):
        """
        Level up the Experience object.
        This causes to update the value of experience for the next level and
        to trigger the levelUp for the owner class (if it exists)
        """
        self._level += levels
        log.debug('Level up! Level: {l}. exp required for next: {e}'
                  .format(l=self._level, e=self._exp_to_level_up))

        # update the experience needed to change level
        self._exp_to_current = self._exp_to_level_up
        self._exp_to_level_up = self._calc_exp_to_next(self._level)

        try:
            # try to level up the owner instance (usually a Character)
            self.owner.levelUp(levels)

            # log it
            log_m = '{oN} gained enough experience for level {l}. '\
                    'Experience for next level: {e}'
            log_m = log_m.format(oN=self.owner.name,
                                 l=self.level,
                                 e=self._exp_to_level_up)
            log.info(log_m)

        except AttributeError, e:
            # if there is no owner or the owner has no levelUp method
            log.error('Experience owner does not exists or does '
                      'not have a levelUp method. {}'.format(e))

            # raise AttributeError(e)

    def _calc_exp_to_next(self, currentLevel):
        """ Calculate how much experience is needed for the next level up. """
        next_level = currentLevel + 1
        return next_level * currentLevel * 500

if __name__ == '__main__':
    import sys
    import random
    sys.path.insert(0, '.')
    import context
    from game import Character

    a = Character()

    # a.levelUp(2)
    while a.level < 5:
        exp_given = random.randint(25, 100) * random.randint(1, 8)
        a.experience.gain(exp_given)

        curr_exp = 'current exp:{e} ({p}%)'
        curr_exp = curr_exp.format(e=a.experience.current,
                                   p=a.experience.percentage)
        next_lev = 'next level:{l} -> exp: {e}'
        next_lev = next_lev.format(l=a.experience.level + 1,
                                   e=a.experience._exp_to_level_up)

        print '>> gained: {} EXP'.format(exp_given)
        print a
        print curr_exp
        print next_lev
        print '---'
