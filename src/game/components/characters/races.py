"""
 Race generator class.
"""
if __name__ == '__main__':
    from os.path import abspath
    import sys
    sys.path.insert(0, abspath('.'))
    import context

import logging
import database

log = logging.getLogger(__name__)


class Race(object):
    """
    Generate a new race with the given name using races.json
    If the race is not found, fallback race is 'Human'.

    :param str raceName: name of the race to create

    """

    __defRace = 'human'

    def __init__(self, raceName, *args, **kwargs):
        """
        Constructor.
        """

        self.__raceData = None
        # Ability bonuses are set to 0 by default.
        # Each race will overwrite them as needed once the race data has been
        # retrieved from the database.
        self._abilityBonus = {
            'strength': 0,
            'dexterity': 0,
            'constitution': 0,
            'intelligence': 0,
            'wisdom': 0,
            'charisma': 0}

        # get the race requested from the database or get the default is fail
        self._getRace(raceName)
        # get the attributes from the race object and assign them as object
        # attributes.
        self._setAttibutes()
        log.debug('New Race object created for a "%s"' % raceName)

    @property
    def name(self):
        """
        Name of the race.
        """
        return self._name

    @property
    def size(self):
        """
        Character size.
        """
        return self._size

    @property
    def abilityBonus(self):
        """
        Ability bonuses given by the race.
        Applied at character creation.
        """
        try:
            return self._abilityBonus
        except:
            return {}

    @property
    def favoredClass(self):
        """
        Favored class of this race,
        Used in the future for character development and leveling up.
        """
        return self._favoredClass

    def _getRace(self, raceName):
        """
        Get the requested race from the database.
        Get the default race if the requested does not exists.
        """
        raceName = raceName.lower().replace(' ', '-')

        # get the available races from the database
        availableRaces = database.getList('races', 'slug')

        # if the requested race is not found set the default race
        if raceName not in availableRaces:
            log.error('"%s" not available. Fallback to "%s". Choose from %s'
                      % (raceName, self.__defRace, availableRaces))
            raceName = self.__defRace

        # load the race resource
        self.__raceData = database.get('races', raceName)

    def _setAttibutes(self):
        """
        Extract the info that we need from the raceData dictionary for a
        more easy access.
        """

        race = self.__raceData
        self._name = race['name']
        self._size = race['size'].lower()
        self._favoredClass = race['favored_class']

        # cycle through the abilities and set the race specific bonuses. if
        # an ability has no bonus it will be removed from the dictionary.
        for ability in [ab for ab in self._abilityBonus]:
            if race[ability] == 0:
                del self._abilityBonus[ability]
            else:
                self._abilityBonus[ability] = race[ability]


if __name__ == '__main__':
    log.setLevel(logging.INFO)
    race = Race('Half orc')
    print race.name, race.abilityBonus
