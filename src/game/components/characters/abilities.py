""" Ability and Abilities classes for characters and monsters alike. """

if __name__ == '__main__':
    import sys, os.path
    sys.path.insert(0, os.path.abspath('.'))
    import context

import random
from collections import OrderedDict
from game.components.dice import Dice
from game.components.constants import MINIMUM_INITIAL_ROLL, ABILITIES


class Ability(object):
    """This is an ability object. Every Character has some.
    It is composed of .value and .mod, that are used all the time in the game.
    They are meant to be wrapped by the Abilities class that will handle them
    for the players.

    name        the name of the ability
    value       the initial value of the ability
    """
    def __init__(self, name, value):
        self._name = name
        self._value = value
        self._updateMod()

    def __str__(self):
        return '[%s] %s (%s)' % (self.name, self.value, self.mod)

    def __setitem__(self, value):
        self._value += value
        self._updateMod()

    def __getitem__(self, value):
        if value == 'mod' or value == 'modifier':
            return self._mod
        elif value == 'value':
            return self._value
        else:
            raise AttributeError('Ability "%s" has no attribute "%s".'
                                 % self.name, value)

    @property
    def name(self):
        """The name of this ability. """
        return self._name

    @property
    def value(self):
        """The value of this ability. """
        return self._value

    @property
    def mod(self):
        """The ability modifier of this ability. """
        return self._mod

    def _updateMod(self):
        self._mod = self._handleModifier()

    def _handleModifier(self):
        """Calculate the ability modifier for the given ability. """
        return int((self.value - 10) / 2)

    def update(self, value):
        """Update this ability by the given value, incrementing or decrementing
        it by <value>. """
        self._value += value
        self._mod = self._handleModifier()


class Abilities(object):
    """Handle the abilities of a character, setting and returning the values,
    the modifiers and updating everything when needed. """
    def __init__(self, abilities=None, raceBonus=None,
                 primary=None, secondary=None, **kwargs):

        # Wrapper dictionary for all the Ability objects,
        # ordered as they are added, as in the constants.ABILITIES tuple
        self._abilities = OrderedDict({})

        # level of the abilities, updated when leveling up
        self._level = 1

        # store primary and secondary stats that will handle priority in
        # assignment of abilities and their leveling up
        self._primaryStat = primary
        self._secondaryStat = secondary

        # Create the abilities
        self.__generate(abilities, **kwargs)
        # add the racial bonuses
        self.__setRacialBonus(raceBonus)

    def __iter__(self):
        return iter(self._abilities.items())

    def __len__(self):
        return len(self._abilities)

    def __contains__(self, item):
        return item in self._abilities

    def __getitem__(self, ability):
        return self.__getAbility(ability)

    def __getattr__(self, ability):
        return self.__getAbility(ability)

    # TODO: Block cls.<ability> = <value> or make it use the proper update
    #       method. this should be done with defining __setattr__ but it loops!

    def __str__(self):
        string = ''
        for ability in self._abilities:
            item = self[ability]
            string += '%s: %s (%s), ' % (ability, item.value, item.mod)

        return string[:-2]  # remove the last space and comma

    def _setAbility(self, ability, value):
        try:
            self.update(ability, value)
        except KeyError:
            self._createAbility(ability, value)

    def __getAbility(self, ability):
        try:
            return self._abilities[ability]
        except KeyError:
            return KeyError('No %s ability exists' % ability)

    # Utility to generate the initial ability values using a 4d6 - lower roll
    # rule
    def __rollInitialValues(self):
        """Roll the initial values for one ability. This could have been done
        with a more simple random.randint, but this is more fun and  accurate.
        """

        d6 = Dice(6)
        # loop until we have at least a value of 8 for the statistic
        while True:
            # this will be our rolls total after we purge the rolls of the
            # lowest value
            totalValue = 0
            # roll 6 dices and sort them from lower to highest value
            rolls = d6.roll(4, getRolls=True)
            rolls.sort()
            rolls = rolls[1:]

            for roll in rolls:
                totalValue += roll

            # check that the result of the rolls is high enough to be used and
            # not have ability values too low to play with.
            if totalValue >= MINIMUM_INITIAL_ROLL:
                break

        del d6
        return totalValue

    def __getAbilityFrom(self, container, ability):
        """Try to get the ability requested from the container passed or return
        False if something goes wrong (i.e. not found). """
        value = None

        # If the container is None there is no sense to go on trying
        if not container:
            return value

        try:
            # try getting the ability from the container with the given ability
            value = container[ability]
        except KeyError:
            try:
                # try checking if the <container> has the ability with its
                # full name, checking the keys first three characters
                # against name (which has three characters)
                name = next(
                            (key for key in container if key.title() == ability[:3]),
                            None)

                value = container[name]
            except KeyError:
                # if nothing has been found, value remains None and will be
                # returned as it is.
                pass

        return value

    def __sortedAbilities(self):
        """Try to sort the abilities for the assignment of the initial roll,
        prioritizing the primary and secondary stats passed, that will receive
        the higher values. """
        ab_list = [x for x in ABILITIES]
        try:
            # try to move the primary statistic in the first position
            ab_list.insert(0, ab_list.pop(ab_list.index(self._primaryStat)))
        except:
            pass

        try:
            # try with secondary stat in second position
            ab_list.insert(1, ab_list.pop(ab_list.index(self._secondaryStat)))
        except:
            pass

        return ab_list

    # generate the ability dictionary for each self.abilities tuple object
    def __generate(self, abilities, **kwargs):
        """Generate or set the abilities.
        If a value is passed then use that, else generate with dice rolls.
        """

        # LIST with the abilities sorted by priority (primary and secondary) as
        # specified when creating the object.
        sortedAbilities = self.__sortedAbilities()

        # Prepare sorted rolls that will be used where a value is not specified
        rolls = [self.__rollInitialValues() for _ in range(6)]
        rolls = sorted(rolls, reverse=True)

        # temporariry dictionary with all the rolls
        temp_abilities = []

        for name in sortedAbilities:
            idx = sortedAbilities.index(name)
            # TODO: do HERE the checks in the next for loop
            # temp_abilities[name] = rolls[idx]

            # try to get the value from kwargs named arguments => priority 1
            value = self.__getAbilityFrom(kwargs, name)

            # if value wasn't found in kwargs try getting from the abilities
            # parameter => priority 2
            if not value:
                value = self.__getAbilityFrom(abilities, name)

            # if the value for this ability is still missing, get correspondend
            # (same index) roll from the rolls list as fallback.
            if not value:
                value = rolls[idx]

            # Since the maximum ability score allowed for level 1 is 18 and all
            # characters are created as level 1 Character, check the value
            # before adding it
            # NOTE: This may be modified in the future to allow higher starting
            #       values (i.e. for monsters, special characters etc...) but
            #       for that we can always use the update method to force set
            #       a higher value
            if value > 18:
                print 'uhuh, you tried to be smart on the %s ability uh? ' \
                      'You cannot set more than a natural 18!' % name
                value = 18

            temp_abilities.append((name, value))

        # create Ability object in the correct order
        # NOTE: This is super ugly. needs some refactoring
        for abilityName in ABILITIES:
            # use the ABILITIES tuple for the order
            for couple in temp_abilities:
                # use the tuple items to create the ability...
                if couple[0] == abilityName:
                    # ...if the tuple ability name == abilityName in ABILITIES
                    self._createAbility(couple[0], couple[1])
                    break

    # Apply the race subclasses racial bonuses to the abilities, following
    # the 3.5 D&D bonuses.
    def __setRacialBonus(self, raceBonus):
        """Apply racial bonuses from the various race subclasses.
        Each <Race> subclass that has bonuses should store them into
        raceBonus as a <dict> of <name> : value (<int>)
        """
        try:
            # Try to access self.raceBonus
            for ability in raceBonus:

                if ability in ABILITIES:
                    self.update(ability, raceBonus[ability])

        except TypeError:
            # if there is no raceBonus attribute then do nothing
            pass

    # Create a new <ability> with <value>
    def _createAbility(self, ability, value):
        """Create a new ability (or reset an existing one) setting the passed
        value as its value. Also update the modifier. """
        self._abilities[ability] = Ability(ability, value)

    # Remove <ability> from the internal dictionary
    def _remove(self, ability):
        """Remove the given <ability> from the abilities dictionary. """
        try:
            del self._abilities[ability]
        except KeyError:
            print 'Can\'t delete "%s" since it doesn\'t exist. ' % ability

    # Call the updater of the ability
    def update(self, ability, value=1):
        """Update the given ability by the value. """
        try:
            self[ability].update(value)
        except KeyError:
            return KeyError('Abilities update could not find %s ability'\
                            % ability)

    def levelUp(self, levels, ability=None):
        """Level up the abilities of a Character if needed.

        Check the levels difference and do an ability increase every 4 levels
        (lv 4, 8, 12...)

        If ``ability`` (``str``) is passed, that ability will gain 1 point.
        If left None the function will try to determine the right one to
        increase, going first on the primary, then secondary, then trying to
        average the other values up to the other ones. This will allow to have
        some sort of balanced statistics with higher values for primary and
        secondary, if they exists.

        In the future We may want to add some function to define some leveling
        behaviour like *balanced* or *power* or whatever, that changes the
        logic used when leveling.

        :param int levels: Current level of the Character

        :param str ability: (optional) ability to increase

        """
        # if no levels gained, stop here.
        if levels == 0:
            return

        # TODO: Define priority for the abilities when going on lowest
        #       * Constitution is important to everybody
        #         so Constitution min >= 10
        #       * ...?

        # constants to check differences against
        MIN_MAX_DIFF = 8
        PRIM_SEC_DIFF = 2
        # string used in while debugging as __main__ module
        debug_string = ''

        # for each level gained increase the current local _level andtry
        # the abilities increment, checking if _level is a multiple of 4 (else
        # continue to the next iteration)
        for _ in range(levels):

            # increase _level by 1 for each element of the range
            self._level += 1

            # if current level is not a multiple of 4 go ahead with the next
            # level up if it exists
            if self._level % 4 != 0:
                if __name__ == '__main__':
                    print '[%s] No ability increase' % self._level
                continue

            # User asked to levelup with a specific ability.
            # Do that and stop here
            if ability:
                self.update(ability)
                continue

            # check constitution and increase that if lower than 10, using a
            # random with ~ 33% probability to update it, because constitution
            # is a (extra) key ability for everybody, since it gives health.
            con = self._abilities['Constitution']
            if con['value'] < 10:
                if random.random() > 0.33:
                    self.update('Constitution')
                    continue

            """
            Following is an algorithm to get the most average ability values
            but pointing to increase primary and secondary first. The idea
            is to don't have too much difference between highest and lowest
            scores, not have negative modifiers but still pointing high for
            primary statistic.

            More logics may come in the future that will be set when creating
            the character (automatically or by hand) so that we can for example
            care less about lower statistics and pump up primary and secondary
            only, or focus on a given one (even though it's not important) and
            max that out.
            """


            # locally store the couple abilityName, value as list of tuples as
            # (<AbilityName>, <value>)
            values = [(k, self._abilities[k]['value']) for k in self._abilities]

            # extract the highest and lowest values from current ability values
            # Highest should be the primary ability for character while lower
            # COULD be Charisma or Wisdom.
            highest = max(values, key=lambda item: item[1])
            lowest = min(values, key=lambda item: item[1])

            if highest[1] - lowest[1] >= MIN_MAX_DIFF:
                # If the difference between highest and lowest score is over
                # the match value, update the lowest one.
                debug_string = '\033[94mupdating lowest ability\033[0m: %s'\
                               % lowest[0]

                self.update(lowest[0])
            else:
                # if diff is lower than match
                try:
                    # Try to work with primary and secondary if they exists
                    primary = self[self._primaryStat]
                    secondary = self[self._secondaryStat]

                    if primary['value'] - secondary['value'] <= PRIM_SEC_DIFF:
                        # update primary ability
                        self.update(self._primaryStat)
                        debug_string = '\033[92mupdating primary ability'\
                                       '\033[0m: %s' % self._primaryStat

                    else:
                        # update secondary ability
                        self.update(self._secondaryStat)
                        debug_string = '\033[93mupdating secondary ability'\
                                       '\033[0m: %s' % self._secondaryStat

                except:
                    # if there is some issue with primary and secondary (maybe
                    # they don't exist) go with the highest instead.
                    self.update(highest[0])
                    debug_string = '\033[92mupdating ability'\
                                   '\033[0m: %s' % highest[0]

            if __name__ == '__main__':
                print '[%s]' % self._level, debug_string

        return True

if __name__ == '__main__':
    def showLevelUpLogic():
        """Test leveling up logic with a prioritized Abilities object with
        Dexterity as primary and Intelligence as secondary. """

        abilities = Abilities(
            primary='Dexterity', secondary='Intelligence')

        print '+++ Testing ability leveling up logic +++\nStats at level 1:'
        print abilities

        abilities.levelUp(19)

        print abilities

    showLevelUpLogic()
