if __name__ == '__main__':
    from os.path import abspath
    import sys
    sys.path.insert(0, abspath('.'))
    import context

import logging
import database
from game.components.items.equipment import Weapon, Armor

log = logging.getLogger(__name__)


class Armory(object):
    """
    Factory and container for Weapon and Armor objects.
    Is used to handle creation and assignment of equipment to any object that
    has an Inventory (in <instance>.inventory), like Characters of containers
    of sort.

    A ``tuple`` of every item that has been created by the Armory
    instance is accessible from the ``items`` property.

    """

    def __init__(self, name='Armory'):

        self.name = name
        # Empty dictionary that will contain all the references to the items
        # we can create, as tuple of tuples with (name, slug) for each item.
        self._list = {}

        # contains a list of all the items created by the armory, so that it's
        # easier to do cleanup
        self._created = []

        self._setup()

        # get the data and generate the instances of Weapon and Armor objects
        # self._createWeapons()

        log.info('New Armory "%s"object created successfully' % name)

    def _setup(self):
        # populate self._list tuples
        self._generate_list('weapon')
        self._generate_list('armor')

    @property
    def items(self):
        """
        Returns a tuple of all the item instances created by this armory.
        """
        return tuple(self._created)

    @property
    def weapons(self):  # NEW
        """
        Return a tuple with the names of the weapons available in the database
        that can be used to create new weapons or reference to existing ones
        """
        return tuple(w[0] for w in self._list['weapon'])

    @property
    def armors(self):  # NEW
        """
        Return a tuple with the names of the weapons available in the database
        that can be used to create new weapons or reference to existing ones
        """
        return tuple(w[0] for w in self._list['armor'])

    def _format_name(self, string):  # NEW
        """
        Format and return the given string as an item name with the first
        letter capital and all the others lower.
        """
        return string[0].title() + string[1:]

    def _format_slug(self, string):  # NEW
        """
        Format and return the given string as an item slug with all the letters
        lower case and spaces replaced with '-'.
        """
        return string.lower().replace(' ', '-')

    def _generate_list(self, itemType):  # NEW
        """
        Get a list of all the weapons available in the database.
        List will be saved as a tuple of tuples (<name>, <slug>)

        .. example::

           (('Composite longbow', 'composite-longbow'), ('Club', 'club'), ...)

        """
        names = database.getList(itemType)
        slugs = database.getList(itemType, 'slug')

        # zip the tuples together as in ((name, slug), (name, slug), ...)
        # and save the tuple inside self._list[<itemType>]
        self._list[itemType] = tuple(zip(names, slugs))

    def _validate_item_name(self, itemName, itemType=None):  # NEW
        """
        Validate a requested item's name comparing it with the names and the
        slugs contained in our local tuples (i.e. _list['weapon']).

        :param str itemName: Name (or slug) for the requested item.

        :param str itemType: Refers to one of the equipment categories
                             contained in the Armory.
                             If None is passed, the function will loop all
                             of the available types.

                             **Accepted values**

                             * 'weapon'
                             * 'armor'  *coming soon*
                             * ...

        :returns: ``True`` if match is found, ``False`` if not found
        """
        dataTuple = None

        try:
            dataTuple = self._list[itemType]
        except KeyError:
            if itemType is None:
                pass
            else:
                msg = 'No "{type}" type of items found in the armory.'
                msg = msg.format(type=itemType)
                log.error(msg)

        def loop(itemName, dataTuple):
            # loop through a tuple of tuples to match the passed name
            for tup in dataTuple:
                # Format the passed string as if it was a slug string and
                # check it against the tuple 'list' <itemType>.
                # If a match is found return True
                realSlug = self._format_slug(itemName)

                if realSlug == tup[1]:
                    return True

            # If no match is found return False
            return False

        if dataTuple:
            return loop(itemName, dataTuple)
        else:
            found = False
            for type_tup in self._list:
                found = loop(itemName, self._list[type_tup])
                if found:
                    break

            return found

    def _validate_item_type(self, itemType):  # NEW
        """
        Validate a requested item type.

        :returns: ``True`` if found and valid, ``False`` if not
        """

        item_slug = self._format_slug(itemType)

        for item_type in self._list:
            if item_slug == item_type:
                return True

        # if we looped through all the item types and did not found a match
        # notify with an error log and return False
        m = ('Item type "{0}" does not exist in Armory. '
             'Available types: {1}'.format(itemType,
                                           self._list.keys()))
        log.error(m)
        return False

    def _create_weapon(self, weapon_data):  # NEW
        """
        Create and return a Weapon object from a dictionary describing it
        """
        weapon = weapon_data

        name = str(weapon['name']).strip().lower()
        description = str(weapon['description'])
        cost = str(weapon['cost'])
        damage = str(weapon['damage'])

        weaponFamily = str(weapon['weapon_family'])
        weaponType = str(weapon['weapon_type'])
        # damageType = str(weapon['damage_type'])

        # TODO: Add damageType to the new weapon once it has been
        #       implemented in the Weapon class

        new_weapon = Weapon(name=name, descr=description, value=cost,
                            damage=damage, weaponFamily=weaponFamily,
                            weaponType=weaponType)

        return new_weapon

    def _create_armor(self, armor_data):  # NEW
        """
        Create and return an Armor object from a dictionary describing it.
        """
        armor = armor_data

        name = str(armor['name']).strip().lower()
        description = str(armor['description'])
        cost = str(armor['cost'])
        armorClass = int(armor['armor_class'])
        armorType = str(armor['armor_type'])
        maxDex = int(armor['max_dexterity'])

        new_armor = Armor(name=name, descr=description, value=cost,
                          armorClass=armorClass, armorType=armorType,
                          maxDexBonus=maxDex)

        return new_armor

    def _get_owned_items(self, owner=None, category=None, name=None,
                         *args, **kwargs):  # NEW
        """
        Return a tuple of item instances owned.

        All arguments are optional. if anything is passed, the function will
        return all the owned items, else it will return them filtered by what
        has been passed (provided that the passed values are correct after
        internal validation).

        If the requested itemType or itemName are invalid the function will
        notify that as an error log and the filter disregarded (all types or
        all names searched.)

        If the requested owner is not a string or does not have a ``name``
        property, a ``TypeError`` will be raised. There is no check on the
        validity of a string, so if a passed name is wrong, it will return 0.

        :param owner: Instance or name of a supposed owner

        :param str itemType: type of the equipment to filter

        :param str itemName: name of the equipment to filter
        """
        # default value for item type and name.
        # False means we are not filtering by that value or the requested
        # type is not valid.
        valid_type = False
        valid_name = False
        valid_owner = False

        # logging string
        message = 'Found {qty} %s%sowned %sin the armory'

        def format_log_string(m):
            # initial format the given string using the global variables set
            # when setting filters
            m = m % ('"{iType}" ' if valid_type else 'item(s) ',
                     'named "{iName}" ' if valid_name else '',
                     'by "{oName}" ' if valid_owner else '')
            return m

        if not owner and not category and not name:
            # If no filter are passed, everything created inside the armory
            # will be returned by the loop, so we can just return all the
            # created items that have an owner of any kind and stop the
            # function here, avoiding time and memory loss.
            haveOwner = []
            for item in self._created:
                # get all the items owned by something
                if item.owner is not None:
                    haveOwner.append(item)

            # format the string for the logging
            m = format_log_string(message).format(qty=len(haveOwner))
            log.info(m)
            # return the count.
            return tuple(haveOwner)

        # If one or more filter is passed proceed setting them up and filtering
        # the items

        # setup owner filter
        if owner:
            if type(owner) is str:
                # If the passed owner argument is a string,
                # suppose that it's a correct owner's name and set that
                valid_owner = owner
            else:
                # if not, try to get the owner's name from the name property
                try:
                    # try to get the owner's name
                    valid_owner = owner.name
                except AttributeError:
                    pass

            # TODO: use try/except/finally for all the steps
            # Finally check for valid_owner to be a string.
            # If not, raise TypeError
            if type(valid_owner) is not str:
                # At this point we should have a string as <owner>.
                # If that's not the case, raise a TypeError
                log.error('%s is not a valid owner or owner name' % owner)
                raise TypeError('%s is not a valid owner to match.' % owner)

        # setup item type filter
        if category:
            if self._validate_item_type(category):
                # if an item type is passe setup the filtering for that,
                # formatting the correct item type name or logging and error
                # if the type is false.
                valid_type = self._format_slug(category)
            else:
                m = 'Error in finding {} owned {}. Searching all types'
                m = m.format(category, owner)
                log.error(m)

        # setup item name filter
        if name:
            if self._validate_item_name(name):
                valid_name = self._format_slug(name)
            else:
                # log warn the the passed name does not exists and we will be
                # looping all items (of the opt. given type for the opt. owner)
                log.warn('"%s" is not a valid name for an armory item.'
                         ' Searching all names.' % name)

        # return values list, contains matching elements.
        retVal = []

        # loop the created items and filter them out
        for item in self._created:
            # if an item has no owner, the item has no owner so, since we are
            # checking for owned items, the loop will stop for this item and go
            # to the next
            if item.owner is None:
                continue

            # Result for checks... True by default, since check can be passed
            # if a value has not been provided. Will be set to false in case
            # a match is required but not found.
            ownerFound = True
            typeFound = True
            nameFound = True

            # check owner's name if owner was passed
            if valid_owner is not False:
                try:
                    if item.owner.name != valid_owner:
                        # if owner's name match set it to true
                        ownerFound = False

                except AttributeError:
                    # This may be because item does not have an owner for some
                    # reason. In this case we just ignore the item
                    pass

            # check item type if itemType was passed
            if valid_type is not False and item._equip_type != valid_type:
                # if the type does not match, set typeFound to false,
                # filtering out the item
                    typeFound = False

            if valid_name is not False and item._rawName != valid_name:
                nameFound = False

            # evaluate filters and append the item to the return values
            # if all the filters match.
            if ownerFound and typeFound and nameFound:
                retVal.append(item)

        m = format_log_string(message)
        m = m.format(qty=len(retVal), oName=valid_owner,
                     iName=valid_name, iType=valid_type)
        log.info(m)

        # return the generated list as a tuple
        return tuple(retVal)

    def _get_not_owned_items(self, category=None, name=None, *args, **kwargs):
        """
        Returns a list of all not owned items created by this armory.
        Results can be filtered by category (weapon, armor...) or by name.
        """
        valid_category = False
        valid_name = False

        warn_log_msg = 'requested {t} {n} not found in armory. looping all'
        info_res_msg = ('Found {qty} not owned %s%s in armory.'
                        % ('"{type}" ' if category else 'item(s) ',
                           'named "{name}" ' if name else ''))

        # setup category filter
        if category:
            if self._validate_item_type(category):
                valid_category = self._format_slug(category)
            else:
                log.warn(warn_log_msg.format(t='type', n=category))

        # setup name filter
        if name:
            if self._validate_item_name(name):
                valid_name = self._format_slug(name)
            else:
                log.warn(warn_log_msg.format(t='name', n=name))

        # start searching
        retVal = []
        for item in [i for i in self._created if i.owner is None]:
            catFound = True
            nameFound = True

            if valid_category:
                if item._equip_type != valid_category:
                    catFound = False

            if valid_name:
                if item._rawName != valid_name:
                    nameFound = False

            if catFound and nameFound:
                retVal.append(item)

        log.info(info_res_msg.format(qty=len(retVal),
                                     type=category,
                                     name=name))
        return tuple(retVal)

    def give(self, itemType, itemName, owner):  # NEW
        """
        Create a new instance of the item of type <itemType> with the given
        name and assign it to the owner as long as it has space inside the
        inventory.

        :returns: `True` if succeded, `False` if not.
                  function can fail silently if:

                  * owner doesn't have an inventory
                  * owner's inventory does not have enough space

                  In any other failure case it will raise some errors.


        :raise: * `TypeError` if something went wrong with the creation,
                * `ValueError` if the requested item does not exist in the
                  requested table (i.e. 'asdfds' in 'weapon').
                * `KeyError` if the requested itemType does not exist in the
                  armory.

        In the future it will try to assign it as equipment first, if the owner
        can use that weapon and the weapon's slot is not used.
        """

        itemType = itemType.lower()

        # Check if the owner has an inventory.
        # If not, return False
        if not hasattr(owner, 'inventory'):
            message = '{ownerName} does not have an Inventory'
            message = message.format(ownerName=owner.name)
            log.error(message)
            return False

        # Check if the inventory of the owner has enough free space.
        # If not, return False
        if owner.inventory.freeSpace == 0:
            log.error('{0} doesn\'t have enough free space'.format(owner.name))
            return False

        # If the requested item does not exist in the database return False
        if not self._validate_item_name(itemName, itemType):
            m = 'Requested {0} {1} does\'t exists'.format(itemType, itemName)
            log.error(m)
            raise ValueError(m)

        # Retrieve the data for the requested weapon and generate a new
        # instance of Weapon
        try:
            item = self.create(itemType, itemName)
        except:
            m = 'Error while creating {0} from {1} table for {2}.'
            m = m.format(itemName, itemType, owner.name)
            log.info(m)
            raise TypeError(m)

        # give the item to the owner
        owner.inventory.add(item)
        # change ownership on the item
        item.changeOwner(owner)
        # update the _owned dictionary
        # self._add_owned_item(itemType, item._rawName, owner.name)

        log_msg = '{n} received a new {t} - {i}'.format(n=owner.name,
                                                        i=item._rawName,
                                                        t=itemType)
        log.info(log_msg)

        return True

    def create(self, itemType, itemName):  # NEW
        """
        Create an instance of the correct type for the item requested, where
        <itemType> is the type of equipment and <itemName> is the name.

        Used internally to create and give new instances of equipment to owners
        that have an inventory (Characters, containers etc...)

        :param str itemType: the type of item to create.
                             **Accepted values**

                             * `weapon`
                             * `armor`

        :param str itemName: The desired item to create

        :returns: `instance` of the requested type if creation went through

        :raise: `KeyError`
        """
        # couple the creator functions with the item type so that it can be
        # called correctly
        creator = {
            'weapon': self._create_weapon,
            'armor': self._create_armor
            }

        itemType = itemType.lower()
        try:
            # if the creator function is not found notify and raise
            _creator = creator[itemType]

        except KeyError, error:
            m = ('Armory cannot create instances of {0}. Accepted values are '
                 '{1}'.format(itemType, str(creator.keys())))
            # log the error and raise a KeyError
            log.error(m)
            raise KeyError(m, error)

        if self._validate_item_name(itemName, itemType):
            item_data = database.get(itemType, self._format_slug(itemName))

            # return the created instance from the creator function after
            # adding it to the _created list
            item = _creator(item_data)
            self._created.append(item)
            return item
        else:
            msg = '{name} doesn\'t exists in {type}.'.format(name=itemName,
                                                             type=itemType)
            log.error(msg)
            raise KeyError(msg)

    def find(self, method, *args, **kwargs):
        """
        Get method to call semantically some other methods.
        Every method accepts different arguments, catched by args and kwargs.

        :param str method: the internal method semantic shortcut to call.

        All other parameters will be passed as ``args`` or ``kwargs`` at the
        internal method, in the order described as following.

        **Accepted method names and arguments**

        * ``owned``:

          * ``owner``: instance of a owner (i.e. a ``Character``)
            or owner's name as ``str``
          * ``category``: ``str`` the equipment type (weapon, armor...)
          * ``name``: ``str`` the name of the equipment to search


        * ``not owned``:

          * same as ``owned`` but the ``owner`` argument.

        """
        # TODO: ADD DOCUMENTATION
        functions = {
            'owned': self._get_owned_items,
            'not owned': self._get_not_owned_items
            }

        try:
            # assume that the method requested exists and try to return its
            # value
            return functions[method](*args, **kwargs)

        except KeyError, error:
            # If the method's name does not exist notify and raise
            msg = '{method} getter not found in Armory. Available are {av}.{e}'
            msg = msg.format(method=method, av=functions.keys(), e=error)
            log.error(msg)

            raise KeyError(msg)

    def _cleanup(self):
        """
        Cleanup armory content stored in self._created.
        Every object that has no reference will be deleted from memory.
        An INFO log will be called, stating how many items are left.
        """

        raise NotImplementedError('armory._cleanup is buggy and may be'
                                  ' deprecated in the future. do not use.')

        log.info('Cleaning up Armory...')

        # delete all items created from the armory that have no reference
        # anywhere.
        for i, item in enumerate(self._created):
            log.warn('trying to delete %s (owner: %s)'
                     % (item.name, item.owner))

            del self._created[i]

        log.info('Armory has been cleaned up. %s item(s) left'
                 % len(self._created))


if __name__ == '__main__':
    from game import Character

    def test():
        log.setLevel(logging.INFO)
        # # create instances
        armory = Armory()
        b = Character(fname='john', lname='doe', gameClass='rogue')
        c = Character(fname='jane', lname='doe', gameClass='fighter')

        # create a weapon with no owner
        armory.create('weapon', 'composite shortbow')

        # create and assign some weapons
        armory.give('weapon', 'dagger', b)
        armory.give('weapon', 'dagger', c)
        armory.give('weapon', 'shortbow', b)
        armory.give('weapon', 'longsword', c)
        armory.give('weapon', 'composite longbow', c)
        armory.give('armor', 'padded', b)
        armory.give('armor', 'chain-shirt', b)

        log.info('Armory total created items: %s' % len(armory.items))

        # test find('owned') and find('not owned') methods
        armory.find('owned', b, 'weapon')
        armory.find('owned', owner='John Doe', name='shortbow')
        armory.find('owned', owner=c.name, category='weapon')
        armory.find('owned', owner=c, category='weapon', name='longsword')
        armory.find('owned')
        armory.find('not owned', category='weapon')
        # armory._cleanup()

        print b.inventory

    test()
