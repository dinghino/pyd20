""" Players and NPC Class. """
if __name__ == '__main__':
    import os.path, sys
    sys.path.insert(0, os.path.abspath(''))
    import context


from os.path import sep
import random
import json
import re
import database
from game.components.characters import BaseClass
from game.components.utilities import Item, Purse
from game.components import constants, Dice
import logging

log = logging.getLogger(__name__)

# Store the names dictionary from names.json when and if a name randomization
# is called.
namesData = None


def getRandom(choice, gender=None):
    """Get randomized gender, name, surname, race and game class for for a
    Character when called on initialization. Can also be used standalone.

    :param choice: Property to randomize. can be one of

                         * ``gender`` either **Male** or **Female**
                         * ``name`` first name based on gender
                         * ``surname``  (second name and) surname for character
                         * ``race`` the name of one of the races, that can be
                           used to generate a :class:`Race` object
                         * ``classname`` name of one of the GameClasses that
                           can be used to generate a :class:`GameClass` object

    :type choice: ``str``

    """
    choice = choice.lower()

    # access namesData as global variable
    global namesData

    try:
        gender = gender.title()
    except:
        pass

    # to ask for a name we have to provide a gender. Check if it exists
    if choice is 'name' and not gender:
        raise TypeError('Cannot get a name without specifying the gender.')

    # if we are asking for a name or surname fetch the data from the names.json
    # inside resources/ if it isn't already been accessed and stored inside
    # namesData
    if (choice == 'name' or choice == 'surname') and (type(namesData) != dict):
        namesFile = open(constants.GAMEDATA_FOLDER + sep + 'names.json', 'r')
        namesData = json.load(namesFile)
        namesFile.close()

    def get_gender():
        """Get a random gender. """
        return random.choice(constants.AVAILABLE_GENDERS)

    def get_name():
        """Get an appropriate name depending on the Character gender. """
        return random.choice(namesData['Firstnames'][gender])

    def get_surname():
        """Get a surname with an optional random middle name. """
        v = ''
        if random.random() > 0.5:
            v += random.choice(namesData['Lastnames']['Second']) + ' '

        v += random.choice(namesData['Lastnames']['First'])

        return v

    def get_race():
        """Get a race name for the Character. """
        return random.choice(database.getList('races'))

    def get_className():
        """Get the name for a game class for the Character. """
        return random.choice(constants.AVAILABLE_GAMECLASSES)

    choices = {
        'gender': get_gender,
        'name': get_name,
        'surname': get_surname,
        'race': get_race,
        'classname': get_className
    }

    # Execute the binded function and return the returned value
    return choices[choice]()


class Character(BaseClass):
    """Class for Player(s) and NPCs.

    At this development stage almost all the functionalities are inside the
    base class, but in the future we will have multiple types of Characters
    (i.e. Monsters or Animals etc) that will follow the same basic rules but
    will need different actions and methods.

    :param gender: Character's gender
    :type gender: ``str``

    :param race: The race of the caracter
    :type race: ``str``

    :param fname: First name
    :type fname: ``str``

    :param lname: Last name
    :type lname: ``str``

    :param abilities: Some - or all - the fixed abilities to generate the
                      Character as in ``{'int': 17, 'cha': 16}``.
                      Can be omitted.
    :type abilities: ``dict``

    :param inventorySize: The slots available in the character inventory
    :type inventorySize: ``int``

    .. note:: **Generating abilities**

        To use predefined ability values there are two options:

        * use the ``abilities`` argument
        * pass them by name as ``kwargs`` in the form of

        >>> str=16, con=13, ...

        Priority is ``**kwargs`` => ``abilities`` => random generated.
        This allow customization of a character or complete random values.
        What is not provided will be randomized.


    .. seealso::
        For a race list see the races documentation. (coming soon).
        Anyway you can consider the Player's Handbook races to play with.

    """
    def __init__(self, gender=None, race=None, gameClass=None,
                 fname=None, lname=None, abilities=None,
                 inventorySize=64, *args, **kwargs):

        self._characterType = 'Humanoid'
        # TODO: add subtype (i.e. race name or elemental type when creating)

        self._gender = gender or getRandom('gender')
        self._gender = self._gender.title()

        # assign the race. if provided use that, else randomize it
        raceName = race or getRandom('race')
        self._setCharacterRace(raceName)

        # assign the gameClass. if provided use that, else randomize it
        className = gameClass or getRandom('classname')
        self._setCharacterClass(className)

        fname = fname or getRandom('name', self._gender)
        lname = lname or getRandom('surname')

        # assign a coin purse
        self._purse = Purse(self)

        # add the initial money to the character
        self._startingMoney()

        # TODO: Add game class before calling super and setting everything else
        #       so that we access the class properties.
        super(Character, self).__init__(
            fname=fname.title(), lname=lname.title(),
            abilities=abilities, inventorySize=inventorySize, *args, **kwargs)

        log_m = 'New Character created: {n}, {g} {r} {c}'
        message = log_m.format(r=self.race.name, c=self.gameClass.name,
                               g=self.gender, n=self.name)
        log.info(message)

    @property
    def gender(self):
        """Character gender. can be either *Male* or *Female*. """
        return self._gender

    @property
    def purse(self):
        """ The character :class:`game.components.utilities.purse.Purse`. """
        return self._purse

    def _startingMoney(self):
        """Roll the starting money for the character, depending on the
        gameClass selected. """

        # multiply the roll result with this
        factor = 10
        dice = Dice(self.gameClass._startingMoney)

        initialMoney = dice.roll() * factor

        self.purse.add('%s gp' % initialMoney)

    # Try to attack the given target
    def attack(self, target):
        """Attack a given target.

        .. warning::
            Not yet implemented here. Will probably reside in the main class.
        """
        pass

    #
    # NOTE: buy() and sell() will be later removed from here and moved inside
    #       a Transaction class to handle them properly.
    #

    def buy(self, item, quantity=1):
        """Buy items paying its price if enough money is available.
        For now this function is in Character class but, later on, we will
        create some sort of Interaction class to handle what two Characters
        can do (i.e. buy, sell, steal, fight, ...).

        :param item: The item to buy
        :type item: :class:`game.components.item.Item`

        :param quantity: How many items the character would like to buy
        :type quantity: ``int``

        :returns: ``tuple`` (item (:class:`game.components.item.Item`),
                             amount (quantity of items bought `int`),
                             money paid (`str` as in ``3 GP``))

        .. note::
            Quantity is a *request* argument. the function check if there
            are actually enough money in the character's purse to buy that
            amount as well if there is enough space in the inventory to store
            them.
        """
        # get the total amount of coins of <coinType> that the character has
        # to pay to buy the item
        itemValue = re.match(constants._MONEY_REGEX, item.value)
        singlePrice = int(itemValue.group(1))
        price = singlePrice * quantity
        coinType = itemValue.group(2)

        payment = 0
        itemsBought = 0

        # available coins of <coinType> that this character have.
        # NOTE: We will have to try and handle change, for example you Could
        #       pay with 1 silver for an item that costs 60 copper and get 40
        #       copper as change
        availableCoins = self.purse[coinType]

        if availableCoins >= price:
            # we have enough money to buy <quantity> <item>(s), so we can try
            # to buy that amount of stuff. We still need to check if we have
            # enough free space to do so.
            addedItem, itemsBought = self.inventory.add(item, quantity)
            # recalculate the price using the real amount of items bought from
            # the character
            price = singlePrice * itemsBought

            # notify that we didn't have space for the requested quantity and
            # we bought a different amount
            if itemsBought < quantity:
                print '%s had space for only %s %s not for %s ...' % \
                      (self.name, itemsBought, item.name, quantity)

            # if we bought stuff, remove the money from the purse
            if itemsBought > 0:
                payment = self.purse.remove('%s %s' % (price, coinType))

                transaction = '%s %s' % (payment, coinType)

            print '%s bought %s %s for %s' \
                  % (self.name, itemsBought, item.name, payment)
        else:
            # we don't have enough money to buy this much items!
            canAfford = int(availableCoins / singlePrice)
            print '%s doesn\'t have enough money to buy %s %s.' \
                  ' Can afford at most %s of them.' % (self.name,
                                                       quantity,
                                                       item.name,
                                                       canAfford)

            return (None, '0 CP', 0)

        # if everything went ok and Character bought stuff we return some info
        # for the seller so that it can adjust its own properties.

        return (item,         # item instance to be used again
                itemsBought,  # quantity of items really bought
                transaction)  # total value of the transaction

    def sell(self, item, quantity=1):
        """Sell item(s) from the inventory and get some money out of it.
        Works very similar as `buy`.

        Check if item is an actual ``Item`` object and that the inventory
        contains the required quantity. If not sell everything.

        :param item: The Item to sell
        :type item: :class:`game.components.utilities.item` instance

        :param quantity: How many item to sell
        :type quantity: ``int``

        :returns: ``tuple`` (``Item`` instance, money gained, quantity sold)
                  This can be used to double check a buy/sell between two
                  Characters

        """
        if not isinstance(item, Item):
            # if <item> is not an instance of Item (i.e. is None) don't do
            # anything and break the function here
            print '%s is not an Item' % item
            return

        if not self.inventory[item]:
            print '%s doesn\'t have any %s in inventory.' \
                   % (self.name, item.name)
            return (0, 0, 0)

        stored = self.inventory.count(item)

        if stored < quantity:
            print '%s have %s %s in inventory, so can\'t sell %s of them.' \
                  % (self.name, stored, item.name, quantity)

            quantity = stored

        itemValue = re.match(constants._MONEY_REGEX, item.value)

        price = int(itemValue.group(1)) * quantity
        coinType = itemValue.group(2)

        # money gained from this sell
        transaction = '%s %s' % (price, coinType)

        # remove the item(s) from the inventory and add the money
        self.inventory.remove(item, quantity)
        self.purse.add(transaction)

        print '%s sold %s %s for %s' \
              % (self.name, quantity, item.name, transaction)

        return (item,         # item instance to be used again
                quantity,     # quantity of items effectively sold
                transaction)  # total value of the transaction


if __name__ == '__main__':
    def test_buy_sell():
        a = Character(lname='Wells', gameClass='rogue')
        i = Item('Shiny Thing', 'So shiny...', '15 gp')
        # correct pronoun for the character
        noun = 'He' if a.gender == 'Male' else 'She'

        print a, '|', a.purse
        print '%s found a %s... %s\ wants LOTS of them!'\
              % (a.f_name, i.name, noun)

        a.buy(i, 3)

        a.sell(i, 1)

        print '%s is left with...' % noun, a.purse

    # test_buy_sell()
    a = Character()
    print a
