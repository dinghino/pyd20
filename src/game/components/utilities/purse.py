
if __name__ == '__main__':
    import sys
    from os.path import abspath
    sys.path.insert(0, abspath('.'))
    import context

import logging
import database
import re
from game.components.constants import _MONEY_REGEX

log = logging.getLogger(__name__)
log.setLevel(logging.INFO)


class Purse(object):
    """A wrapper object that handle money divided in coin types.

    :param owner: instance of the owner
    :type owner: `class`

    :param initialMoney: Money that the purse is created with.
                         **Not implemented yet. instead add manually**
    :type initialMoney: n/a

    """
    def __init__(self, owner=None, initialMoney=None):

        self._coins = ()
        # contains all the coins owned by the Purse
        # and gets updated on every transaction.
        self._money = {}

        # setup initial content of the purse
        self.setup()

        self._owner = owner
        log.debug('New Purse created.')

    def __getitem__(self, name):
        try:
            # try to get the coins value using the name as it is passed
            return self._money[name]
        except:
            # if not found, try find the name type using MONEY_TYPE.
            # The idea is that we asked for, 'CP' or 'cp' instead of 'copper'
            name = self._findMoneyType(name)[0]
            return self._money[name]

    def __str__(self):
        string = ''
        for coin in self._coins:
            string += ' %s %s' % (self[coin['slug']], coin['shortname'])

        return string

    def setup(self):
        """
        Setup the Purse instance using the retrieved data from sqlite db.
        """
        self._coins = tuple(database.get('currencies'))

        for i in self._coins:
            self._money[i['slug']] = 0

    @property
    def owner(self):
        """
        Return the instance of the owner of the Purse object.
        """
        return self._owner

    @property
    def copper(self):
        """
        Returns the quantity of copper pieces in the purseself.
        """
        return self._money['copper']

    @property
    def silver(self):
        """
        Returns the quantity of silver pieces in the purseself.
        """
        return self._money['silver']

    @property
    def gold(self):
        """
        Returns the quantity of gold pieces in the purseself.
        """
        return self._money['gold']

    @property
    def platinum(self):
        """
        Returns the quantity of platinum pieces in the purseself.
        """
        return self._money['platinum']

    @property
    def total(self):
        """
        Return ``int`` for the total value in copper coins of all the coins in
        the purse. Used to evaluate payment options and confront purses.
        """
        total_copper = 0
        # loop through every coin type owned and get
        # the total value of the purse
        for ct in self._coins:
            # convert the owned coins in copper and sum to the total value
            total_copper += self._money[ct['slug']] * ct['value']

        return total_copper

    def _validate_money_type(self, coinType):
        """
        Validate the passed coinType string and, if correct, return the
        matching database object describing the coin.

        :param str coinType: the string to validate as coin type

        :returns: ``None`` if not found or the found money type from
                  _findMoneyType
        """

        # get the dict that describe the coin type for the value passed
        found = self._findMoneyType(coinType)

        if found:
            return found

        else:
            m = ('Purses cannot handle "{type}" as currency. '
                 'Available are {av}.')

            message = m.format(type=coinType,
                               av=[i['slug'] for i in self._coins])

            log.error(message)
            raise ValueError(message)

    def _findMoneyType(self, string):
        """Find the tuple contained in the money constants in constants.py that
        contains the requested money type.

        :param str string: coin full name, shortname or slug

        :returns: ``None`` if not found or ``dict`` of the coin type if found.
        """
        # normalize the string format
        if len(string) <= 2:
            s = string.upper()
        else:
            s = string.lower()

        # find the coin using the passed string and comparing it to the coins
        # full names, short names and slugs.
        # store in <found> if... found.
        found = None
        for c in [i for i in self._coins]:
            fn, sn, sl = c['name'], c['shortname'], c['slug']
            if s.title() == fn or s.upper() == sn or s.lower() == sl:
                found = c
                break

        return found

    def _parseMoneyString(self, string):
        """Use regex _MONEY_REGEX to match and parse the given string into a
        value and a coin type to add or remove money from the purse.
        """

        match = re.match(_MONEY_REGEX, string)

        if not match:
            raise ValueError('Purse can\'t parse "%s"' % string)

        coins = match.group(1)
        coinType = match.group(2)

        return (coins, coinType)

    def _add(self, amount, slug):
        """
        Add some money to the given <slug> type.
        Validation should be done before calling as this is an internal method.

        :param int amount: the amount of coins to add
        :param str slug: the coin type slug

        :returns: ``int`` money added
        """
        self._money[slug] += amount

        return amount

    def _remove(self, amount, slug):
        """
        Remove some money from the given <slug> type.
        Validation should be done before calling as this is an internal method.

        If amount > coins available for the requested type, it will remove
        everything it has and return ...

        :param int amount: the amount of coins to remove
        :param str slug: the coin type slug

        :returns: money actually removed.
        """
        current = self._money[slug]

        if amount > current:
            amount = current

        self._money[slug] -= amount

        return amount

    def has(self, value):
        """
        Return a boolean stating if the purse has at least <value> coins, where
        <value> is a money string (<coin> <coinType>).
        This is a check for EXACTLY that amount on that type and does not count
        other type of money. To do so use ``canPay()``
        """
        coins, coinType = self._parseMoneyString(value)

        coin = self._findMoneyType(coinType)

        if not coin:
            raise ValueError('Purses cannot handle "%s".' % coinType)

        # return True if purse has at least <coins> of type <coinType>
        #        False if it has less.
        return self._money[coin['slug']] >= int(coins)

    def canPay(self, value):
        """
        Evaluate if the purse can pay the require value using any type of coins
        available.
        """
        if self.has(value):
            # if the purse has the needed money as they are asked it can pay.
            # Return True.
            return True

        coins, coinType = self._parseMoneyString(value)
        coin = self._validate_money_type(coinType)

        if not coin:
            raise ValueError('cannot')

        # required copper to fulfill the payment
        required_copper = coin['value'] * int(coins)

        return self.total >= required_copper

    def add(self, value):
        """Add some money to the purse.

        :param str value: ``30 gp`` or ``15 silver``.
                          String formatted as ``<value> <coin type>``

        """

        coins, coinType = self._parseMoneyString(value)

        # find the requested coin type in the constants
        found = self._findMoneyType(coinType)

        if not found:
            raise ValueError('I don\'t know what to do with %s!' % coinType)

        # force <quantity> as integer
        quantity = int(coins)
        # add the money in the correct place
        self._add(quantity, found['slug'])

    def remove(self, value):
        """Remove some money offrom the purse.

        For the moment it will remove exactly what asked (if available).

        In the future it will probably do some operation to remove the desired
        value using all the coin types if needed.
        This may be done using another method (like a pay()) and is still in
        development so be aware.

        :param str value: ``30 gp`` or ``15 silver``.
                          String formatted as ``<value> <coin type>``

        """
        moneyLoss = 0
        coins, coinType = self._parseMoneyString(value)

        # get the dict that describe the coin type for the value passed
        found = self._validate_money_type(coinType)

        # force the coins count as integer
        quantity = int(coins)

        # remove the money lost for the current operation
        moneyLoss = self._remove(quantity, found['slug'])

        # format the string to be returned as money string
        retVal = '{} {}'.format(moneyLoss, coinType.upper())

        return retVal

    def addBunch(self, strings):
        """
        Add some types of coins with a single tuple of strings
        formatted as money strings "<value> <coinType>".
        """
        if type(strings) is not tuple:
            raise TypeError('purse.addBunch requires list or tuple to work. '
                            'got %s instead' % type(strings))

        for string in strings:
            self.add(string)

    def pay(self, value):
        """
        Attempt to pay the requested quantity of money using all the coins
        available.
        This means that if we ask for 12 GP to a purse that has 10 GP and 50 SP
        it will be able to pay 10 GP and 20 SP in total.

        :param str value: Money string that we require to pay
        """

        if self.has(value):
            # If the purse contains the amount of the required type of coins
            # remove those and stop here.
            return self.remove(value)

        if not self.canPay(value):
            # Lazily evaluate if it's possible to pay in some way.
            # If that's not the case return False and stop here
            return False

        # serialize the value string
        coins, coinType = self._parseMoneyString(value)

        # get the dict that describe the coin type for the value passed
        found = self._validate_money_type(coinType)

        if not found:
            raise TypeError('specified coin type value "%s" cannot be found.'
                            % coinType)

        # find the index of the found coin description inside the tuple.
        currency_idx = self._coins.index(found)

        # At this point we know that the purse DOES NOT have at least the
        # required coins of the required type, so we can assume that that type
        # of coins will be emptied out and something else will go too.
        # NOTE: For the moment we'll not consider paying with something of
        #       higher value since that would require handling change from
        #       the seller (or whatever). We'll implement it in the future.

        # Money strings generator, to create the return values.
        def gen_str(val, slug):
            return '{0} {1}'.format(val, slug)

        # First we convert the required money in coppers, so we can do math
        # with the various types
        remainder = int(coins) * found['value']

        # TODO: To pay with higher value coins we should decide if we want to
        #       first check lower ones and use higher before or do the
        #       opposite. This will change the order on which the function will
        #       work.

        # list that will be filled with tuples of money string to remove
        # the coins from the purse
        coins_to_remove = []
        retVal = []

        while remainder > 0 or currency_idx >= 0:
            _coinDict = self._coins[currency_idx]

            # start by trying to get all the money required from required type.
            owned_money = self._money[_coinDict['slug']] * _coinDict['value']

            # this is what remain to pay after we emptied the required
            # coin type. Value is expressed in copper coins.
            remainder -= owned_money

            if remainder < 0:
                owned_money += remainder
                remainder = 0

            # return owned money of the evaluated type in their native value.
            # i.e. GP in copper 3000 => 30
            realValue = owned_money / _coinDict['value']

            # Generate the money string and append to the list that will
            # be returned by the function.
            retVal.append(gen_str(realValue, _coinDict['shortname']))

            # append the tuple with (int, str) for value and type to be
            # internally removed by _remove (less operations)
            coins_to_remove.append((realValue, _coinDict['slug']))

            # evaluate the cheaper coin type index and get it from _coins
            # to check if they are enough to pay the remained
            currency_idx -= 1

            # cheapest coins are copper coins at index 0, so we can't go lower.
            # if we haven't reached the required amount there is some problem.
            if currency_idx < 0:
                break

        if remainder > 0:
            log.warn('cannot pay the {v} with {p} for now. Payment with higher'
                     ' value coins not yet implemented as it requires change '
                     'handling. This will come soon.'.format(v=value, p=self))
            return tuple()

        for removed in coins_to_remove:
            self._remove(removed[0], removed[1])

        return tuple(retVal)

if __name__ == '__main__':
    p = Purse()
    p.add('30 gp')
    p.addBunch(('150 cp', '45 sp', '10 PP'))
    print p.remove('2 gp')
    # p.remove('50 gp')
    # p.add('30 gold')
    # p.add('1platinum')
    # p.remove('150 gold')
    # print testPurse
    # print testPurse.total
    print 'purse status:', p
    print 'paid 35GP: %s' % (p.pay('35 gp'),)
    print 'remaining after payment of 35GP:', p
