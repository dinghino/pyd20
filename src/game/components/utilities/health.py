"""Health objects handle the HP of a character or anything else (even a door). """
if __name__ == '__main__':
    from os.path import abspath
    import sys
    sys.path.append(abspath('.'))
    import context

from game.components import Dice


# TODO: total hp should never go lower than 1 hp even with constitution mod
#       too low.

class Health(object):
    """Health object, describing and handling health pools for everything.

    :param str hitDice: valid ``Dice`` string name used for the instance
                            values generation

    :param int modifier: modifier applied to the health. commonly a
                         constitution modifier from a Character's Abilities

    """

    def __init__(self, hitDice='d6', modifier=0):
        # dice used to roll HPs
        try:
            self._dice = Dice(hitDice)
        except:
            self._dice = hitDice

        # modifier applied to health
        self._modifier = modifier

        # level of the health object
        self._level = 1
        # Health can't go lower than this
        self._min_hp_value = -10

        self._setUp()

    def __str__(self):
        return str(self.current) + '/' + str(self.total)

    @property
    def current(self):
        """Current hp count. """
        return self._current

    @property
    def total(self):
        """Total hp available. """
        return self._total

    def _setUp(self):
        """Called on init to initialize everything correctly. """
        # calculate the total value of the dice considering extra rolls and
        # extra bonus
        dice_value = (self._dice.faces * self._dice._rolls) + self._dice.bonus

        self._total = dice_value + self._modifier
        self._current = self._total

    def changeModifier(self, newModifier):
        """Evaluate delta between current and passed modifier and apply
        to the current health status.

        :param int newModifier: New value to use as modifier

        .. note::
           This function is used when levelling up the Health object and can
           also be used when the owner (i.e. a ``Character``) loses for some
           reason points to Constitution, reducing its bonus; in this case the
           function will use the Health current level and modify the HP pool by
           the ``delta modifier`` (passed - current) * self._levels.

           So if a Character loose 1 constitution bonus and is a level 4 - so
           its Health property is at level 4, it loses 4 HP in total.

        """

        try:
            d_modifier = newModifier - self._modifier
            # update local _modifier
            self._modifier = newModifier
            # add the delta of the modifier value * levels done to the total hp
            self._total += d_modifier * self._level
            self._current += d_modifier * self._level
        except TypeError:
            # This happens when newModifier is None which shouldn't be but can
            # happen...
            pass

    def levelUp(self, levels=1, newModifier=None):
        """Level up the health. Used by Character to increase health properties
        when levelling up.

        :param int levels: How many levels we need to levelUp

        :param int newModifier: The (optional) new modifier to apply to the
                                health pool

        """

        if levels == 0:
            # no level up needed if level increase is 0
            return

        # update current _level value adding levels
        self._level += levels

        for _ in range(abs(levels)):
            # for each level gained increase total and current hp by random
            # value using dice.roll and adding the modifier

            add_hp = self._dice.roll() + (self._modifier)

            if add_hp <= 0:
                # we always add at least 1 HP per level
                add_hp = 1

            # add delta_hp to the current hp if Health is full
            self._current += add_hp if levels > 0 else -add_hp

            # add to the total hp
            self._total += add_hp if levels > 0 else -add_hp

            if newModifier != self._modifier:
                # update modifier and hp based on that if the modifier passed
                # is different than the one already stored.
                self.changeModifier(newModifier)

    def remove(self, value):
        """Remove some hp from current if possible.

        Can be used when the owner (i.e. a ``Character``) receives some damage
        of any kind.

        :returns: self.current if success or ``False`` if not possible. """

        if self._current > self._min_hp_value:
            self._current -= value
            if self._current < self._min_hp_value:
                # if we went lower than the lowest possible, set that instead
                self._current = self._min_hp_value

            return self.current

        else:
            # can't go lower than current
            return False

    def add(self, value):
        """Add some hp to current if possible.

        Can be used when the owner (i.e. a ``Character``) receives some healing
        of any kind.

        :returns: self.current if success or ``False`` if not possible.
        """
        result = 0
        c = self._current  # temp for current health
        t = self._total    # temp for max health
        if self._current != self._total:
            self._current += value

            if self._current >= self._total:
                self._current = self._total

            result = value - (t - c)  # real hp added

            return result
        else:
            # can't add hp since they are at maximum
            return result


if __name__ == '__main__':
    hp = Health('1d6', 1)
    print 'initial health 1d6 + 1 constitution modifier:', hp
    hp.remove(3)
    print 'lost 3 hp:', hp
    hp.levelUp(3, 2)
    print 'gained 3 levels and +1 con modifier (total 2)\n'\
          '\t> +3d6 + 2 + 1*4 = (16 < x > 31):', hp
    hp.changeModifier(0)
    print 'lost 2 (+2 => 0 -> -8HP) con modifier:', hp
