"""Define the inventory class that will contain all the items that something can
have, being a character, a chest, a monster or whatever. """

if __name__ == '__main__':
    from os.path import abspath
    import sys

    sys.path.insert(0, abspath('.'))
    import context

import re
from game.components.constants import _MONEY_REGEX
from item import Item


class Inventory(object):
    """Inventory for Characters or anything else.
    For now it includes a Purse object that is the character purse, but it's
    not sure if that will remain there or not. """

    def __init__(self, owner=None, maxCapacity=64):
        """ Constructor for Inventory object.

        :param owner: the character that has this inventory
        :type owner: :class:

        :param maxCapacity: The maximum capacity of the inventory in slots
        :type maxCapacity: :class:`int`

        """
        self.content = {}
        self._maxCapacity = maxCapacity
        self._owner = owner

    def __iter__(self):
        # Iterator for the inventory object
        return iter(self.content.items())

    def __len__(self):
        # total amount of items in inventory
        totalItems = 0
        for item in self.content:
            totalItems += self.content[item]['qty']

        return totalItems

    def __contains__(self, item):
        # True if inventory has <item> in self.content
        return item._rawName in self.content

    def __getitem__(self, item):
        """You can ask for an item passing it's name as a string or passing
        directly the object that describes it.
        If no item is found the inventory say so and return None
        (not throwing errors). """
        obj = None
        try:
            # try getting the item name from item, considering it an instance
            # of Item
            name = item._rawName
        except AttributeError:
            # if not found, try considering it a string with the item's name
            # and format it properly to retrieve the item
            try:
                name = item.strip().lower()
            except:
                raise TypeError('Error in retrieving %s. was expecting Item'
                                ' instance or string')
        finally:
            try:
                # try to retrieve the
                obj = self.content[name]['item']
            except KeyError:
                pass

        # if we haven't found the object in the inventory try to access direct
        # attribute of the inventory (such as the money purse)
        if not obj:
            try:
                obj = getattr(self, item)
            except:
                return KeyError('%s not found in inventory' % item.name)

        return obj

    def __setitem__(self, key, item):
        name = key._rawName
        self.content[name] = {'item': item}

        return self[item]

    def __str__(self):
        """Printing the inventory of a character will run listItems() and get a
        formatted string with all the character's inventory content and value.
        """
        return self.listItems()

    @property
    def owner(self):
        """Object related to this instance of Inventory.

        A ``Character`` has its inventory inside the property ``.inventory``
        """
        return self._owner

    @property
    def _ownerName(self):
        return self._owner.name if self._owner else ''

    @property
    def itemsTotalValue(self):
        """ Get the total value of the items in inventory as a dictionary. """

        totalValue = {}

        for item in self.content:
            value, coinType = self._getItemTotalValue(item)

            try:
                totalValue[coinType] += value
            except KeyError:
                totalValue[coinType] = value

        return totalValue

    @property
    def countItems(self):
        """``int`` Total count of the items in the inventory.

        Works as len(Inventory)

        """
        return len(self)

    @property
    def capacity(self):
        """``int`` Capacity of the inventory in slots. """
        return self._maxCapacity

    @property
    def freeSpace(self):
        """``int`` Available free space in the Inventory. """
        return self.capacity - self.countItems

    def count(self, item):
        """Count how many items of the given type are in the inventory. """
        name = self._serializeItemName(item)
        try:
            amount = self.content[name]['qty']
        except:
            amount = 0

        return amount

    def add(self, item, quantity=1):
        """Add ``Item`` to the inventory.

        The function will check that the item argument is an instance of
        ``Item`` and that the quantity is at least 1.
        It will also check that there is enough free space in the inventory for
        all the items; if that's not the case it will fill up the inventory as
        much as possible.

        :param item: The item to be added to the inventory
        :type item: :class:`game.components.utilities.item.Item`

        :param quantity: How many items add to the inventory
        :type quantity: ``int``

        :returns: ``tuple`` (item, qty)

                  * item instance
                  * quantity really added

        """
        if self.freeSpace == 0:
            print '%s\'s inventory is full!' % self._ownerName
            return item, 0

        # item must be an instance of Item
        if not isinstance(item, Item):
            raise TypeError('Can\'t add %s to the inventory! That\'s no item!'
                            % item.name)

        # you must add at least 1 item
        if quantity < 1:
            raise ValueError('Can\'t add less than 1 %s. Use remove() instead'
                             % item.name)

        itemName = item._rawName

        if quantity > self.freeSpace:
            # # if we found more items than we can carry the quantity is
            # reduced to the space available in the inventory
            quantity = self.freeSpace

        try:
            self.content[itemName]['qty'] += quantity
        except KeyError:
            self[item] = item
            self.content[itemName]['qty'] = quantity

        return item, quantity

    def remove(self, item, quantity=1):
        """Remove the given quantity of item from the inventory.

        Will check it the item exist in the inventory and how many there are;
        If we have more than *quantity* it will remove those. If there are less
        It will remove all of them.

        :param item: The item to be removed from the inventory
        :type item: :class:`game.components.utilities.item.Item`

        :param quantity: How many items remove from the inventory
        :type quantity: ``int``

        :returns: ``tuple`` (item, qty)

                  * item instance
                  * quantity really removed

        """
        if item not in self:
            string = 'No item "%s" found in %s\'s inventory' \
                     % (item._name, self._ownerName)

            return KeyError(string)

        if quantity < 1:
            return (ValueError('Cannot remove less than 1 %s. use add()'
                               % item.name), 0)

        if self.content[item._rawName]['qty'] <= quantity:
            del self.content[item._rawName]

            # set how many items were effectively removed from the inventory
            quantity = self.count(item)
        else:
            self.content[item._rawName]['qty'] -= quantity

        return item, quantity

    def listItems(self):
        """Generate and return a sort of bill of the inventory, listing
        the content, free and total space and total value of the item stored.

        .. note:: This will be refactored in the future to return something
                  that can be used to actually format a table (in CLI or GUI)

        """
        inventory = self.content
        ownerName = self._ownerName + ' ' or ''
        string = ''
        if len(inventory) > 0:

            string += '###### %sinventory [%s/%s] ######\n' % \
                      (ownerName, self.countItems, self.capacity)

            for item in inventory:
                qty = inventory[item]['qty']
                string += '* [%s] %s \n' % (qty,
                                            inventory[item]['item'].__str__())

            # get the total value of the items and print it too
            invValueString = ''
            totalValue = self.itemsTotalValue

            for coin in totalValue:
                invValueString += ' %s %s' % (totalValue[coin], coin)

            string += '\n'
            string += 'Inventory value:%s\n\n' % invValueString
            string += '###### End %s\'s inventory ######\n' % self._ownerName

        else:
            string += '%s%sinventory is empty!'\
                      % (ownerName, '\'s ' if len(ownerName) > 1 else '')

        return string

    def _serializeItemName(self, item):
        if isinstance(item, Item):
            name = item._rawName
        else:
            name = item

        return name

    def _getItemTotalValue(self, item):
        """Get the item total value using the value of one unit multiplied
        by all the units stored inside the inventory. """
        # serialize the item name if needed
        name = self._serializeItemName(item)

        # match the name with _MONEY_REGEX and get value and coinType
        itemValue = re.match(_MONEY_REGEX, self.content[name]['item'].value)
        try:
            # How much does a unit of this item value
            unitValue = int(itemValue.group(1))

            # How many items of this type we have in inventory
            units = self.content[name]['qty']

            # the coin type for the value of this item
            coinType = str(itemValue.group(2))

            totalValue = unitValue * units

            return totalValue, coinType

        except:
            raise ValueError('Could not get value for %s', name)


if __name__ == '__main__':

    sword1 = Item(name='Rusty Sword',
                  descr='This is a simple rusty sword',
                  value='5 sp')
    sword2 = Item(name='Pointy Sword',
                  descr='This is a very pointy sword',
                  value='1 gp')
    sword3 = Item(name='Shiny Sword',
                  descr='Awwww... so shiny and beautiful!',
                  value='5 gp')

    bread = Item(name='Bread loaf',
                 descr='A piece of soft bread',
                 value='10 cp')

    junk = Item(name='Useless junk',
                descr='REALLY useless junk... Why bother..?',
                value='1 sp')

    print ' --------- inventory testing ---------'

    inv = Inventory()
    # # simple inventory testing
    inv.add(sword1, 5)  # You're out of luck and found 5 rusty swords
    inv.add(sword2)     # no value means only 1! you found a pointy sword!
    inv.add(sword3)     # add one sword
    inv.add(bread, 10)
    inv.add(junk, 5)
    print inv
    print inv.count('useless junk')
    print '>>', inv[junk]
