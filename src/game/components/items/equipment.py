"""Base class for equipments. """

if __name__ == '__main__':
    from os.path import abspath
    import sys
    sys.path.insert(0, abspath('.'))
    import context

import re
from game.components.utilities.item import Item
from game.components.dice import Dice
from game.components import constants


class Equipment(Item):
    """Base class for all equipments. """
    def __init__(self, name, descr, value=0, owner=None):

        super(Equipment, self).__init__(name, descr, value, owner)

        # overwrites Item _item_type property, allowing Character to equip the
        # Item.
        self._item_type = 'Equipment'
        self._type_color = '\033[42;30m'


class Weapon(Equipment):
    """Creates Weapon objects that can be used by Characters. """
    def __init__(self, name, descr, value, damage, weaponFamily=None,
                 weaponType=None, owner=None):

        super(Weapon, self).__init__(name, descr, value, owner)

        # equipment type. used by Characters to equip it in the correct slot
        self._equip_type = 'weapon'
        self._type_color = '\033[46;30m'

        # save extra specific attributes for weapons
        self._damageDice = None
        self._damage = None
        self._extraDamage = None
        self._crit_threat = 20
        self._crit_damage = 2

        # save weapon string information
        self._weaponFamily = weaponFamily
        self._weaponType = weaponType

        # overwrite damage numeric and Dice information
        self._getWeaponDamage(damage)

    def rollDmg(self):
        """
        Roll and get the damage done by a hit from this weapon.
        This won't consider critical hits, since those will be handled from
        the called (an attack method of sort) if needed.
        """
        # primary damage is always rolled
        damage = self._damageDice.roll()

        try:
            # try to roll the extra damage dice. If not exist it will raise
            # an AttributeError since NoneType has no roll method. catch it and
            # do nothing. If succeded, add the damage of the extra dice.
            damage += self._extraDamage.roll()
        except AttributeError:
            pass

        # return the damage (int) rolled.
        return damage

    def _serializeDamageString(self, string):
        """Get all the information for the weapon damage from the passed string
        and set them where they need to be.
        If no match is found raise a ValueError.

        match is composed of 4 groups:
        * damage                => required
        * optional damage       => optional. default is ``None``
        * critical threat roll  => optional. default to 20
        * critical multiplier   => optional. default 2

        :returns: ``tuple`` (firstDice, secondDice, threat, multiplier)

        """

        REGEX = constants._WEAPON_DMG_REGEX
        try:
            match = re.match(REGEX, string)

            firstDice = match.group(1)
            secondDice = match.group(2)
            critThreat = match.group(3) or '20'
            multiplier = match.group(4) or '2'
        except:
            raise ValueError('Error while parsing damage string for weapon %s'
                             % self.name)

        return (firstDice, secondDice, critThreat, multiplier)

    def _getWeaponDamage(self, string):

        damage, extra, threat, critDmg = self._serializeDamageString(string)

        self._damageDice = Dice(damage)
        self._damage = damage

        if extra:
            # if extra damage exists create the damage and add to the string
            # in self._damage
            self._extraDamage = Dice(extra)
            self._damage += '/%s' % extra

        self._crit_threat = int(threat)

        self._crit_damage = int(critDmg)

    def __str__(self):
        sep = ' - '
        string = '%s' % self._damage + sep

        if self._crit_threat < 20:
            string += '%s-20/' % self._crit_threat

        string += 'x%s' % self._crit_damage
        # return string
        return super(Weapon, self).__str__(string)


class Armor(Equipment):
    """Equipable Armors for Characters. """
    def __init__(self, name, descr, value,
                 armorType, armorClass=1, maxDexBonus=0, owner=None):

        super(Armor, self).__init__(name, descr, value, owner)

        # equipment type. used by Characters to equip it in the correct slot
        self._equip_type = 'Armor'
        self._type_color = '\033[43;30m'
        self._armor_type = armorType
        # save extra specific attributes for armors
        self._armor_class = armorClass
        self._max_dexterityBonus = maxDexBonus

    @property
    def armorClass(self):
        """Armor class bonus derived from the armor. """
        return self._armor_class

    @property
    def maxDexterity(self):
        """Maximum dexterity bonus usable on CD rolls when wearing this armor.
        """

        return self._max_dexterityBonus

    def __str__(self):

        string = '- {} Armor - '.format(self._armor_type)
        string += 'AC: {} - Max dex: +{}'.format(self._armor_class,
                                                 self._max_dexterityBonus)

        return super(Armor, self).__str__(string)


if __name__ == '__main__':
    i = Item('Junk', 'This is junk', '1 cp')
    e = Equipment('Equipment', 'This is some generic equipment', '1 sp')
    w = Weapon('Longsword', 'Standard sword', '15 gp', damage='1d8 18 x3')
    w2 = Weapon('Quarterstaff', 'A staff', 0, damage='1d6/1d6 x2')
    a = Armor('Padded armor', 'Simple armor', '5 gp', 1, 8)

    print i   # generic item
    print e   # generic equipment
    print w   # weapon > Longsword
    print w2  # weapon > Quarterstaff
    print a   # armor  > Padded armor

    # testing class inheritance
    print 'inst of Equipment: %s | inst of Item: %s'\
          % (isinstance(w, Equipment), isinstance(w, Item))

    print w.rollDmg()  # test weapon damage
