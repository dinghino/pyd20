"""Dices are the primary utility used through the game logic.
As in the tablegame, we use various faced dices to randomize various actions
results, applying bonuses when needed, if available.
"""

if __name__ == '__main__':
    from os.path import abspath
    import sys
    sys.path.insert(0, abspath('.'))
    import context

import random
import re
from game.components.constants import _DICE_REGEX


class Dice(object):
    """Generate a Dice with the given faces.
    Dices are used for almost everything in the game.

    :param dice: The dice name in all its glory
    :type dice: :class:`str`

    .. note::
       ``dice`` Any of the following examples will generate usable dices:

       * ``1d20``: 1 dice with 20 faces
       * ``d8``: 1 dice with 8 faces
       * ``3d6``: 3 dices with 6 faces
       * ``2d4+2``: 2 dices with 4 faces with a +2 bonus
       * ``d2+1``: 1 dice of 2 faces with a +1 bonus
       * ``12``: 1 dice with 12 faces (can be both ``str`` or ``int``)


    When using ``.roll()`` the dice will roll the quantity before the ``d``
    and will add the optional ``bonus`` on the rolls total, so:

    .. code-block:: python

       dice = Dice('2d6+4')
       results = dice.roll(3, getRolls=True)  # -> [2, 3, 5, 1, 6, 1]
       bonus = dice.bonus  # -> 4

    You can then do operations on the list as you like and add the bonus if
    needed.

    """
    def __init__(self, dice='1d20'):
        """ Constructor. """

        self.__create(dice)

    @property
    def name(self):
        """Composed name of this (set of) dice(s).
        When passing an integer (or a string with only digits) it will default
        to ``1d<value>`` where ``value`` is the dice argument passed When
        creating the dice.
        """
        name = '%sd%s' % (self.qty, self.faces)

        if self.bonus:
            add = str(self.bonus)
            # if the bonus is negative the - is automatic, but we want a '+'
            # when the bonus is positive.
            name += add if self.bonus < 0 else '+' + add

        return name

    @property
    def qty(self):
        """Quantity of dices rolled each time a roll is called. ``.roll(1)``
        for a ``3d4`` will cause to roll a total of 12 dices. """

        return self._rolls

    @property
    def faces(self):
        """Number of faces of the dice. """
        return self._faces

    @property
    def bonus(self):
        """Bonus (positive or negative) applied to the total after rolled the
        dice(s). """
        return self._bonus

    def __create(self, dice):
        """ Extract information about the dice from the dice argument. """

        try:
            # Try to convert to a integer and set that. if there are other
            # symbols other than numbers (i.e. 'd', or a '+' or '-') it
            # will fail and go ahead with the regex validation

            # this could raise a ValueError
            faces = int(dice)

            if faces <= 0:
                raise ValueError('Faces cannot be less than 1!')

            self._faces = faces
            self._rolls = 1
            self._bonus = 0

        except ValueError:
            # At this point we suppose that the dice was created like
            # '1d20' or 'd20' etc, so we validate the string and if OK
            # get the values and set those.

            # Since 0 <int> passes down here, we check for the value instead
            # of failing the regex match.
            if dice == 0:
                raise ValueError('Cannot use 0')

            # validate the regex and get the match object
            match = re.match(_DICE_REGEX, dice)

            if not match:
                raise ValueError('Unexpected string"%s" for Dice. see docs'
                                 ' for dices creation on pyd20.rtfd.io' % dice)

            # rolls number is optional for the regex. can be defaulted to 1
            try:
                self._rolls = int(match.group(1))
            except:
                self._rolls = 1

            # faces are NOT optional
            self._faces = int(match.group(2))

            # bonus is optional
            try:
                self._bonus = int(match.group(3))
            except:
                self._bonus = 0

    def _execRoll(self, times):
        """ Roll <times> * self.qty dices and get the results in a list
        Examples:

        * '1d20' * 2 times (2*1) -> [10, 17]
        * '2d6' * 3 times (2*3)  -> [1, 4, 3, 5, 2, 6]

        :returns: :class:`list`

        """
        result = []
        for roll in range(times * self.qty):
            result.append(random.randint(1, self.faces))

        return result

    def roll(self, rolls=1, getRolls=False):
        """
        Roll dice(s) and return the result(s).

        :param rolls: The number dices to roll
        :type rolls: `int`

        :param getRolls: Wether you want the list of the results or the total.

                        * ``True`` the function will return a ``list`` with all
                          the rolls results.

                        * ``False`` it will evaluate the total and return that
                          as an ``int``.
        :type getRolls: :class:`bool`

        :returns: :class:`list` or :class:`int` depending on ``getRolls``

        .. note::
           If ``getRolls`` is false (default) the function will return an
           ``int`` value corresponding to the sum of all the values **and**
           the bonus of the dices, so

           .. code:: python

              d = Dice('2d6+2')
              d.roll()   # => from (1 + 1 + 2) up to (6 + 6 + 2)
              d.roll(2)  # => from (2 + 2 + 4) up to (12 + 12 + 4)

        """
        # roll an undefined number of dices and get the total result
        results = self._execRoll(rolls)

        # if getRolls is requested return the list of the rolls as INTEGERS
        # instead of just the sum of all of them
        if getRolls:
            return results
            # TODO: return tuple (rolls<list>, bonus<int>) instead of just the
            #       list of rolls...?

        # if getRolls is False (default) calculate and return the sum of all
        # the values plus dice.bonus * number of rolls executed
        total = sum(results) + (self.bonus * rolls)

        return total

    @classmethod
    def getSet(self, *args):
        """Get a set of dices in a dictionary.

        For when more than one dice is needed

        :param args: Contains dice names to create. If nothing is passed the
                     function will generate a standard set of dices.

        Default set is ``1d4`` ``1d6`` ``1d8`` ``1d10``
        ``1d12`` ``1d20`` ``1d100``

        :return: ``dict`` of ``Dice`` s with created dice names as keys like
                 ``1d6`` or ``2d6+3``

        **Example**

        .. code-block:: python

           from game import Dice
           diceSet = Dice.getSet('d4', '1d8+2')

           diceSet['1d4'].roll()  # rolls the 1d4 dice

        """
        # default dice set
        dices = ('d4', 'd6', 'd8', 'd10', 'd12', 'd20', 'd100')

        if args:
            # if some dice names are passed we overwrite the default dices
            # with all the correctly formatted dices in args
            newDices = []
            # create the list with the new dices, using _DICE_REGEX to
            # validate the passed strings
            for d in args:
                d = str(d)
                if re.match(_DICE_REGEX, d):
                    newDices.append(d)
                else:
                    print 'Cannot create dice "%s". invalid format. '\
                          'Maybe missing the "d"?' % d

            dices = newDices

        retDict = {}

        for d in dices:
            # create dices and store them inside the dictionary
            dice = Dice(d)
            retDict[dice.name] = dice

        return retDict


if __name__ == '__main__':
    d = Dice('2d6+3')
    print d.name, [d.roll() for _ in range(10)]

    dSet = Dice.getSet('d20', 8, 'd3')
    print dSet
