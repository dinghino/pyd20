"""Classic fight round.
For now it will concern only two characters at a time, so it's a one on one"""
import time
from dice import Dice 
from character import Character


class Combat(object):
    """Combat will take any number of fighters as <args> and will store them
    in the fighters attribute as long as they have been build using
    Character class or one of its child.
    There aren't any other parameters to set for now for a fight. """
    def __init__(self, *args, **kwargs):
        # here we'll store all the fighters partecipating this fight
        self.fighters = []

        # for every argument in args check if it'sin fact a Character, then
        # add them to the fighters list
        for char in args:
            if self._isCharacter(char):
                self.fighters.append(char)

        # Can't have a fight with less than 2 people, right?
        if len(self.fighters) < 2:
            print 'Cannot fight with less than 2 people!'
            return

        # save the fighters in list that will be overwritten when rolling the
        # initiative for each one of them, sorting them in the proper order
        self.dice = Dice(20)

        self.rollInitiative()

    def _isCharacter(self, item):
        if item.__class__.__name__ == 'Character':
            # if <item> is in fact an object from the Character class => True
            return True
        elif isinstance(item, Character):
            # if it's a subclass of Character => True
            # FIXME: Due to the current stucture of the program this will
            #        return false for now, so the first IF.. statement.
            return True
        else:
            # else => False
            print '%s is not a valid Character subclass object' % item
            return False

    def rollInitiative(self):

        order = []
        for char in self.fighters:
            # calculate the initiative for each character and add the result
            # to the order list
            roll = self.dice.roll() + char.initiative
            order.append(roll)

        # zip and reverse sort the characters using their initiative result
        # to sort them, higher goes first
        sortList = sorted(zip(order, self.fighters), reverse=True)

        # regenerate self.fighters list in the proper order
        self.fighters = [x for (y, x) in sortList]

    def start(self):
        """ Begin the fight with the characters added when creating the
        Combat object. """

        # initial attacker and target indexes inside self.fighters
        currentAttacker = 0
        currentTarget = 1

        # quantity of fighters left in combat
        fightersLeft = len(self.fighters)

        # TODO: when we'll implement weapons we will check for
        #       critical hits and damages. For now we suppose
        #       we have a SWORD that deals 1d8 damages
        dice = Dice(8)

        while fightersLeft > 0:
            """Here we loop until we have 1 fighter only left alive.
            For now we just loop them so that each fighter attacks the next in
            line. Later we'll add some logic to determine the target. """
            attacker = self.fighters[currentAttacker]
            target = self.fighters[currentTarget]

            # ########## #
            # BEGIN TURN #
            # ########## #

            # Since 'unconscious' fighters can be present but CANNOT do anything
            # but die (or get healed by someone) we check if the currently
            # selected attacker is in fact alive.
            # If it's alive it tries to attack <target> (confirming it), else
            # we go to the next
            if attacker.status == 'alive':
                # confirm the target
                target = self.confirmTarget(attacker, target)

                if attacker == target or target == None:
                    print '%s cannot find anybody to kill :(' % attacker.f_name
                    break


                # determine the roll to hit for the attacker
                # NOTE: for now we only do melee attacks, so this is here.
                #       Later on we will have to decide what bonus to use.
                roll_to_hit = self.dice.roll() + attacker.meleeAttackBonus

                # NOTE: if target is unconscious the armor class doesn't count
                #       so the compare value with the roll to hit is set to 1
                #       NOTE that in the future the attacker must NOT roll a
                #       critical failure (natural 1 with the dice). if that
                #       happens no matter what, the hit is a fail.
                threshold = target.ac if target.status == 'alive' else 1
                # compare the roll with the target armor class
                if roll_to_hit >= threshold:
                    # target hit! roll damages!
                    damages = dice.roll()
                    # apply the damages to the target
                    target.damage(damages)
                    print '[%s] hit [%s] for %s damages!' % (attacker.f_name,
                                                             target.f_name,
                                                             damages)
                else:
                    # attack didn't went through.
                    print '[%s] miss [%s] (%s against %s)!' % (attacker.f_name,
                                                               target.f_name,
                                                               roll_to_hit,
                                                               target.ac)
            else:
                pass

            if target.status == 'dead':
                # if the target is dead, remove it from the fighters list
                self.fighters.remove(target)
                fightersLeft = len(self.fighters)
                # NOTE: Debug
                print '--- %s DIED! %s left' % (target.f_name, fightersLeft)

            # ########## #
            #  END TURN  #
            # ########## #

            # set the new attacker and target
            currentAttacker +=1
            currentTarget += 1

            # restart the loop for attackers and targets if the index is
            # higher than the fighters left in the fight
            if currentAttacker >= fightersLeft: currentAttacker = 0
            if currentTarget >= fightersLeft: currentTarget = 0

            # a little pause after each round
            if currentAttacker == 0:
                print 'Round ended!'
                for char in self.fighters:
                    print char
                print '--------------------------'
                time.sleep(.5)

            time.sleep(.1)

        print '--- fight ended! ---'

    def confirmTarget(self, attacker, target):
        """Find another target to attack if the next one is not 'alive'
        or confirm the current target to attack if it's alive. """

        if target.status == 'alive':
            return target
        else:
            target= next((t for t in self.fighters if t.status is 'alive' and t is not attacker), None)

            # if we can't find a target ALIVE it means that they are all
            # dead or incapacitated. Finish them off! break the while loop
            if target == None:
                target = next((t for t in self.fighters if t.status is 'unconscious' and t is not attacker), None)

            return target
