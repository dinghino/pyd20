"""Constants definition for the whole ``components`` module. """

from os.path import abspath, sep

GAMEDATA_FOLDER = abspath('.' + sep + 'src' + sep + 'game' + sep + 'resources')

# the minimum value that a ability can have when rolling the initial dices
MINIMUM_INITIAL_ROLL = 8

# abilities name and descriptio
ABILITIES = (
    # physical strength that influence the melee attacks
    # as well as the weight/quantity of item that the character can
    # carry
    'Strength',
    # influences the ranged attacks and the ability to dodge or perform
    # 'stunt' actions
    'Dexterity',
    # influences the health pool, the armor class and some saving
    # throws against conditions such as poisons, bad weather, etc...
    'Constitution',
    # Will be used later on for magic, languages and most of all ability points
    'Intelligence',
    # used for magic, knowledge etc
    'Wisdom',
    # used for... stuff.
    'Charisma'
)

# Available gennders for characters
AVAILABLE_GENDERS = ('Male', 'Female')

# Available races for the game. used for validation and random selection
AVAILABLE_RACES = ('Human', 'Dwarf', 'Elf', 'Gnome',
                   'Half-Elf', 'Half-Orc', 'Halfling')


# Available game classes. used for validation and random selection
AVAILABLE_GAMECLASSES = ('Barbarian', 'Bard', 'Cleric', 'Druid', 'Fighter',
                         'Monk', 'Paladin', 'Ranger', 'Rogue', 'Sorcerer',
                         'Wizard')

# coins type for Purse(s) arguments validation
MONEY_TYPE = (('copper', 'CP'),
              ('silver', 'SP'),
              ('gold', 'GP'),
              ('platinum', 'PP'))

# regex used to evaluate money strings and extract value and coin type
_MONEY_REGEX = r'(\d+)\s?([a-zA-Z]{2,})?'
"""Used to match strings containing <coin><coinType>.
Any number of digits, optional whitespace, any number of letters
Matches these examples: 30gp, 1 silver, 5 cp, 2492 pp.
value will be in group 1, coin type will be in group 2.
"""

_DICE_REGEX = r'(\d+)?d(\d{1,3})([\+-]\d{0,2})?$'
"""
Regex string used to validate the <dice> property.

Will match string like ``1d20``, ``3d8``, ``2d6+4`` (up to +99), ``d10``.

* ``(\d+)?``: optional quantity of digits as in number of dices that get rolled
  on each ``.roll()`` call.
* ``d``: character **d** that identifies the dice. optional when passing only
  the number of faces
* ``(\d{1,3})``: required number of faces from ``1`` to ``999`` (set max 100)
* ``([\+-]\d{0,2})?``: optional match for bonuses

  * match + or - signs to get the bonus 'value'
  * match any digits up to 99

Is used to validate string-type dice names if it cannot be converted directly
into a single integer. as in ``Dice(8) => '1d8'``

"""

_WEAPON_DMG_REGEX = r'((?:\d+)?d(?:\d{1,3})(?:[\+-]\d{0,2})?)\/?((?:\d+)?d(?:\d{1,3})(?:[\+-]\d{0,2})?)? ?(\d+)?(?:-20)? ?(?:x(\d+))?$'
"""Regex to extract damage information from weapons.
works similar to _DICE_REGEX for validating dice strings, except it produces a
single group for each dice that then needs to be processed by Dice to create it.

It generates [Dice, (Dice), (critRange), multiplier] where the values in () are
optional. if critRange is missing we will se '20' instead.

The two dices are for different normal damage and extra damage (i.e on the
quarterstaff)

Can match strings like

'd8'                => ['d8', None, None, None]
'1d4 x2'            => ['1d4', None, None, '2']
'1d8 18 x3'         => ['1d8', None, '18', '3']
'd4/d8 19-20 x2'    => ['d4', 'd8', '19', '2']

extra damage can be omitted and needs to default to None
critRange can be omitted and needs to default to 20 (20-20)
multiplier can be omitted and needs to default to 2 (x2)
"""
