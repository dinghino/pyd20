"""
Utility module to transfer data from json resources to a sqlite DATABASE.
This module will go once the porting is complete and is not to be imported
for use inside other modules.

This module serves the only function to lazily load stuff into the sqlite db
and regenerating the tables while working on them.
It is NOT to be imported or used externally.

"""
import json
import sqlite3
import uuid
from os.path import join
import sys
import re
from pprint import pprint

sys.path.insert(0, '.')
from src import database

import logging



RES_FOLDER = join('.', 'src', 'game', 'resources')
DB_FOLDER = join('.', 'src')
DATABASE = join(DB_FOLDER, 'db.sqlite')


def _generate_uuid():
    """
    Generate a (sort of) unique id and return it as string.
    """
    return unicode(hex(uuid.uuid4().fields[-1])[:14])


def _getJson(fileName):
    """
    Get the json data from <fileName> (no extension) and return the python
    data structure.
    """
    f = open(join(RES_FOLDER, fileName + '.json'))
    d = json.load(f)
    f.close()
    return d


def _getHeaders(cursor, tableName):
    """
    Get all the headers name in a list.
    """
    cursor.execute('SELECT * FROM %s' % tableName)
    return [descr[0] for descr in cursor.description]


def loadWeapons():
    """delete (if exists) the weapons table, recreate it and load all the
    weapons in weapons.json in the database.
    """

    d = _getJson('weapons')

    db = sqlite3.connect(DATABASE)
    cursor = db.cursor()

    cursor.execute('DROP TABLE IF EXISTS weapon')
    create_weapons = """CREATE TABLE "weapon" (
        `id`	INTEGER PRIMARY KEY AUTOINCREMENT,
        `name`	TEXT NOT NULL UNIQUE,
        `slug`	TEXT NOT NULL UNIQUE,
        `description`	TEXT,
        `cost`	TEXT NOT NULL,
        `damage`	TEXT NOT NULL,
        `damage type`	TEXT NOT NULL,
        `weapon family`	TEXT NOT NULL,
        `weapon type`	TEXT NOT NULL,
        `weight`	REAL NOT NULL,
        `increment`	INTEGER,
        `uuid`	TEXT UNIQUE)"""

    cursor.execute(create_weapons)
    db.commit()

    # headers for the weapons table
    columns = _getHeaders(cursor, 'weapon')

    # normalized data from weapons.json will be stored in this list and will be
    # used to generate the tuples that will be loaded into the database
    data = []

    # Normalize data for sql injection
    for i, weapon in enumerate(d):
        data.append({})
        # TODO: Add slug, lowercase - separated
        data[i]['id'] = i + 1
        data[i]['slug'] = weapon['Name'].lower().replace(' ', '-')
        data[i]['increment'] = 0
        for k in weapon:
            if k == 'Text':
                newK = 'description'
            elif k == 'id':
                newK = 'uuid'
            else:
                newK = str(k.lower())

            data[i][newK] = str(weapon[k])

    # print data

    for weapon in data:
        values = tuple(str(weapon[k]) for k in columns)
        query = 'INSERT INTO weapon VALUES(?{0})'
        query = query.format(',?' * (len(columns) - 1))
        cursor.execute(query, values)

    db.commit()
    db.close()


def loadSizeModifiers():
    """
    Load size modifiers for characters.
    """
    # open json file
    d = _getJson('size_modifiers')
    # db access
    db = sqlite3.connect(DATABASE)
    cursor = db.cursor()

    cursor.execute('DROP TABLE IF EXISTS size_modifiers')

    create_table = """CREATE TABLE "size_modifiers"(
        `id` INTEGER PRIMARY KEY AUTOINCREMENT,
        `name` TEXT UNIQUE NOT NULL,
        `slug` TEXT UNIQUE NOT NULL,
        `modifier` INTEGER NOT NULL,
        `uuid` TEXT UNIQUE NOT NULL)"""

    cursor.execute(create_table)

    columns = _getHeaders(cursor, 'size_modifiers')

    for i, k in enumerate(d):
        query = 'INSERT INTO size_modifiers VALUES(?%s)'\
                % (',?' * (len(columns) - 1))
        values = [i + 1,
                  str(k).title(),
                  str(k).lower().replace(' ', '-'),
                  d[k],
                  str(_generate_uuid())]

        cursor.execute(query, values)

    db.commit()
    db.close()


def loadMoney():
    """
    Load the currencies, names, shortnames and their values for conversion.
    """
    d = ((1, 'Copper', 'CP', 'copper', 1),
         (2, 'Silver', 'SP', 'silver', 10),
         (3, 'Gold', 'GP', 'gold', 100),
         (4, 'Platinum', 'PP', 'platinum', 1000))

    db = sqlite3.connect(DATABASE)
    cursor = db.cursor()

    cursor.execute('DROP TABLE IF EXISTS currencies')
    create_table = """CREATE TABLE 'currencies'(
        `id` INTEGER PRIMARY KEY AUTOINCREMENT,
        `name` TEXT UNIQUE NOT NULL,
        `shortname` TEXT UNIQUE NOT NULL,
        `slug` TEXT UNIQUE NOT NULL,
        `value` INTEGER NOT NULL
    )"""

    cursor.execute(create_table)

    cursor.executemany('INSERT INTO currencies VALUES(?,?,?,?,?)', d)
    db.commit()
    cursor.close()
    db.close()


def loadPlayableRaces():
    """
    Load (some) data for the playable races that can be used to create both npc
    and player's characters.
    """
    d = _getJson('races')[:-1]

    # Serialized data for the races.
    data = []
    abilities = ['strength', 'dexterity', 'constitution',
                 'intelligence', 'wisdom', 'charisma']

    for i, race in enumerate(d):
        data.append({})
        # add the blank abilities modifiers (set 0) that will be later updated
        # if a modifier exists
        for a in abilities:
            data[i][a] = 0

        for key in race:
            value = race[key]
            # exclude the dictionaries from the parsing as they will be handled
            # separately
            if type(value) is dict:
                continue

            newK = str(key.lower())
            value = value

            if type(value) is unicode:
                v = str(value)
            elif type(value) is list:
                v = ','.join(value)
            else:
                v = value

            data[i][newK] = v

        ab_bonuses = race['Traits']['Stats']

        for ab in ab_bonuses:
            data[i][str(ab).lower()] = ab_bonuses[ab]

    db = sqlite3.connect(DATABASE)
    query = "DROP TABLE IF EXISTS races"
    db.execute(query)

    create_table = """CREATE TABLE races(
        `name` TEXT,
        `slug` TEXT,
        `age` TEXT,
        `favored class` TEXT,
        `languages` TEXT,
        `bonus languages` TEXT,
        `alignment` TEXT,
        `base weight` TEXT,
        `height` TEXT,
        `religion` TEXT,
        `size` TEXT,
        `speed` INTEGER,
        `weight modifier` TEXT,"""
    for ab in abilities:
        create_table += '`%s` INTEGER,' % ab
    create_table = create_table[:-1] + ');'

    db.execute(create_table)

    # keys sorted as in the database
    keys = ['name', 'slug', 'age', 'favored class', 'languages',
            'bonus languages', 'alignment', 'base weight', 'height',
            'religion', 'size', 'speed', 'weight modifier'] + abilities

    # create a list of tuples with all the races data sorted correctly
    sql_data = []
    for i, race in enumerate(data):
        raceData = []
        for key in keys:
            if key is 'slug':
                raceData.append(race['name'].lower().replace(' ', '-'))
            else:
                raceData.append(race[key])
        sql_data.append(tuple(raceData))

    sql_inject = "INSERT INTO races VALUES (?{0})"
    sql_inject = sql_inject.format(',?' * (len(keys) - 1))

    print sql_inject

    cursor = db.cursor()
    cursor.executemany(sql_inject, sql_data)
    db.commit()
    cursor.close()
    db.close()

    # TODO: Missng complex race specific bonuses from Traits property
    #       but since they are not used yet this is not a big problem


def loadArmors():
    """
    Load the armors into the sql database
    """
    pass


def loadGameClassesProgression():
    """
    Load the gameClasses objects into the database.
    These objects won't include the level progression for the classes;
    instead they will just have the basic information for creating a level 1
    character of the given class.

    .. note::
       They won't probably include all the bonuses either, at least for now.
       Bonuses will most probably be handled in a separate table called
       class_abilities or something like that where we'll have a column for
       each level and a row for each gameClass, so that we can retrieve the row
       when we level up (or create, for level 1), given the gameClass name

    """

    # NOTE: Table name is 'classes'

    # get all the classes data from the json. exclude the last object since
    # that's the template
    jd = _getJson('classes')[:-1]

    db = database.connect()
    cursor = db.cursor()

    d = database.get('classes')

    # convert from camel case to underscore
    first_cap_re = re.compile('(.)([A-Z][a-z]+)')
    all_cap_re = re.compile('([a-z0-9])([A-Z])')

    def convert(name):
        # Convert from camelcase to underscore
        s1 = first_cap_re.sub(r'\1_\2', name)
        string = all_cap_re.sub(r'\1_\2', s1).lower()
        return string.strip().replace(' ', '_')

    for item in d:
        table_name = item['slug'] + '_progression'
        print table_name

        drop_table = """DROP TABLE IF EXISTS {t}""".format(t=table_name)

        table_name = 'class_progression_' + item['slug']
        create_table = """CREATE TABLE {table}(
        uuid TEXT NOT NULL UNIQUE,
        level INTEGER NOT NULL UNIQUE,
        base_attack_bonus INTEGER,
        fortitude INTEGER,
        reflex INTEGER,
        will INTEGER,
        abilities TEXT,
        PRIMARY KEY(uuid, level)
        )""".format(table=table_name)
        cursor.execute(drop_table)
        cursor.execute(create_table)
        db.commit()

        # build up the list to post into the table
        lev_prog = []
        levels = 20

        # get the correct level progression dictionary from the json using the
        # level and the current item name from the database
        jd_idx = next(i for i, o in enumerate(jd)
                      if o['Class'] == item['name'])
        json_item = jd[jd_idx]

        # loop all the levels and populate the lev_prog list with all the
        # filtered and refactored level progression bonuses
        for i in range(levels):
            _json = json_item['ClassAbilities'][str(i + 1)]

            obj = {
                'uuid': database.generate_uuid(),
                'level': i + 1,
                'base_attack_bonus': _json['BAB'],
                'fortitude': _json['Fort'],
                'will': _json['Will'],
                'reflex': _json['Ref'],
                'abilities': ','.join(_json['Abilities'])
                }

            lev_prog.append(obj)

        database.post(table_name, lev_prog)


if __name__ == '__main__':

    logging.basicConfig(level=logging.DEBUG)
    loadGameClassesProgression()
