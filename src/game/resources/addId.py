import json, uuid

uid = uuid.uuid4()
uid = unicode(hex(uid.fields[-1])[:14])

data = {
  'id': uid
}

for _ in range(5):
    uid = unicode(hex(uuid.uuid4().fields[-1])[:14])
    data['id'] = uid
    print _, data['id']
