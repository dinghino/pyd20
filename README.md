# Python 2.7 _D&D 3.5_ roguelike game


## Description

This project is a learning platform I started working on to learn Python language.
I'm building it with Python 2.7 but will probably move it to Python 3(.5) later on because... yes.

At the current developing stage there is no UI and no actual game, as I'm working on the base classes, implementing the basic rules of the game and management (such inventory, money, character creation and so on) but there is a way to create some sort of _royal rumble_ where some character fight each other to death, which is nice.

## Table of Contents

* [Description](#Description)
* [Table of Contents](#Table-of-Contents)
* [Installation](#Installation)
* [Current development stage](#Current-development-stage)
* [Usage](#Usage)
* [Documentation](#Documentation)
* [Contributing](#Contributing)
* [License](#License)
* [Credits](#Credits)

## Installation

No installation or extra python modules are needed to run the main application.

The admin application is being designed with Qt4 designer and uses `pyqt`, so you need it to run it.
you can do `pip install -r requirements.txt` that should take care of everything.

To compile the documentation locally you will need `sphinx`.

## Current development stage

As of this README update There are a few things available (that may not be in the documentation at this moment):

#### Available classes

* **Character**
* Abilities
* Ability
* Inventory
* Item
* Purse
* **Combat**
* **Dice**

`Character` is the actual class that is developed while the others are utility or test (`Combat`) classes to handle
properties and development of a character.
As of now characters have:
* gender, name, `Race` and `GameClass`
* Health (will be refactored as standalone class)
* `Abilities` (with `Ability` classes)
* saving throws (not fully implemented)
* `Inventory` (usable)
* coin `Purse` (usable)

Characters can be created, specifying every aspect or can be randomized partially or totally.
They can level up (or down) using ``levelUp`` that will handle everything.

As of now there is no way to save or load an existing character but I'm planning to create that, storing as xml or json all the data of a character and allowing to reload it's state.


## Usage

As long as you have Python 2.7 on your machine you can run `main.py` to run what's in there, or see the documentation to try and create your own stuff.

## Documentation

Documentation for this project can be found on [ReadTheDocs.io](http://pyd20.rtfd.io) or can be built withing `./docs` using `make html`.
On readthedocs documentation is automatically updated from `master` branch.

## Contributing

As in the description, this project is a platform of mine to learn the language, so I'd like to keep it '_private_'. This doesn't mean that You can't fork it, suggest functionalities or report issues and so on, just... I'd like to know what's happening since I need to learn :D
If by any chance you do a PR please, please prettyplease document it really well so that I can understand better! :)

## License

This project is under MIT license.

## Credits

Dataset for creation are mainly from [this](https://github.com/zellfaze/dnd-generator) repository, That I kindly thank for the job of putting all of that data together.
