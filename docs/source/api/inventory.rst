Inventory
=========

Description
-----------

Inventories are used to contain :class:`game.components.utilities.item.Item` objects, that can be added or removed from them with the relative class methods.

At the moment each ``Item`` occupies one inventory slot and the (optional) item's weight is not considered at the moment.

Each `Character` object has an Inventory that can be found in the ``.inventory`` property and can be used described in this page.


.. note::

   Inventory will receive a major revamp in the near future, allowing all the object
   instances instead of using a fake counter.

Examples
--------

An inventory can be created as a standalone object but is usually inside another object like a ``Character`` or an ``Item`` (for example a crate or a box).

.. code-block:: python

  # Suppose we have a junk = Item('Useless Junk'...) that costs 1 SP

   from game import Inventory

   inv = Inventory()  # Default size is 64 slots but can be changed

   inv.add(junk, 5)
   print inv  # will output the following, as will inv.listItems()

   # ######  inventory [5/64] ######
   # * [5] Useless junk | REALLY useless junk... Why bother..?
   #
   # Inventory value: 5 SP
   #
   # ###### End 's inventory ######

   inv.count(junk)  # => 5

   inv.remove(junk, 10) # => (junk, 5) where junk is the actual instance
   print inv.listItems() # =>  inventory is empty!


Accessing items
---------------

Inventory class has custom ``__getitem__`` and ``__setitem__`` methods that
allow to interact with the dictionary that contains all the Items.

when Items are stored in an Inventory they  are organized in a dictionary
structured as follow

.. code-block:: python

   {'item name': {'item': <instance>, 'qty', <int> }, ...}

Retrieving an object from the inventory is as simple as asking for the object
name or the instance as item, like

.. code-block:: python

   from game import Inventory, Item

   myInv = Inventory()
   myItem = Item('Useless junk', 'Really useless junk')

   myInv.add(myItem, 10)  # will add 10 myItem to the inventory

   myInv[myItem]          # will return the item instance from the 'item' key
   myInv['useless junk']  # same as the one before, will return the Item

To count how many *Useless junk* we have we can follow the same logic and ask
to

.. code-block:: python

   myInv.count(junk)  # => 10
   myInv.count('useless junk')  # => 10

Note that the name passed when accessing the Item does not have the upper case
**U** at the beginning. That's because internally we serialize the passed name
to match how the items are created so we are sure that it's correctly written.



Class documentation
-------------------

.. automodule:: game.components.utilities.inventory
   :members:
