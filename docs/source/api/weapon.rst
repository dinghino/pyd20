Weapons
=======

Weapons are the primary mean to deal damage to objects in the world, living being or not,
items and so on.


Class documentation
-------------------

Weapon class extends from the Equipment subclass of :class:`game.components.utilities.item.Item`
that is used to specify equipable gear.

.. autoclass:: game.components.items.equipment.Weapon
   :members:


Weapon template
---------------

Each weapon is described inside the ``weapon`` table in the sqlite database and
has these properties.

.. code-block:: python

  {
      "name": "",           # <str> Name of the weapon
      "slug":"",            # <str> Stripped name for reference, similar to web slugs
      "weight": 0,          # <int> Weight in Kilograms
      "cost": "",           # <str> Price in "<qty> <coinType>" as in "15 GP"
      "damage": "",         # <str> Complete damage string (see relative section)
      "description": "",    # <str> Description for the item
      "weapon_family": "",  # <str> General weapon type: Simple, Martial, Exotic
      "weapon_type": "",    # <str> Light, One-handed, Two-handed, Ranged...
      "damage_type": ""     # <str> What type(s) of damage it does
      "increment": 0,       # <int> range increment for distance bonus/malus
      "weight": 0           # <float> weight in kilograms of the item
  }

Damage string
+++++++++++++

The ``damage`` value describe all the information for a medium sized weapon
of that kind as found in the Player's handbook.

When a Weapon is created the Damage string is processed with a *regex* string and
the values are extracted (or defaulted.)

``1d4/1d6 19-20 x3`` is a valid (and complete) Damage string, where you find

* Damage dice(s): formatted as a :class:`game.components.dice.Dice` would expect
  them to be (so a ``d6`` would be fine).
* Critical threat range: the ``-20`` part can be omitted. If all is omitted, the
  default ``20`` is set when creating the weapon.
* Critical multiplier: can be omitted. default x2 will be set.

Valid examples
^^^^^^^^^^^^^^
``1d4 18 x2``, ``d6``, ``1d10+1 x3``, ``d4/d4 x3``...
