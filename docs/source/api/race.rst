Race
====

Description
-----------

In the game world there are a lots of intelligent races. Some of them are playable
races, and are for player's characters creation as well as NPCs creation.
Each race differs from the other in background, bonuses, favorite class (gameClass)
and so on, so the choice of race and gameClass depend on each other.

Available races
---------------

The available races are the ones described in the **Player's handbook 3.5**, and are:

* Human
* Dwarf
* Elf
* Gnome
* Half-Elf
* Half-Orc
* Halfling

For the moment there is no restrictions implemented for race/classes selection.

Creation
--------

Race objects are automatically created inside a ``Character`` when that is created.
A race can be specified passing ``race='<raceName>'`` or randomized by omitting
the argument. If the passed race name does not exists it will default to ``Human``
to avoid errors while setting up and *using* the Character.

.. code-block:: python

   # Creating a Race object is not needed but it can still be done importing
   # the module from the characters/ folder. This can be done for testing purposes
   from game.components.characters.races import Race
   myRace = Race('Human')
   print myRace.name  # => Human

   # Correct use of the Race class is to use it within a Character
   from game import Character

   myCharacter = Character(race='Half-Elf')
   print myCharacter.race.name  # => Half-Elf

   mySecondChar = Character(race='Dog')  # Won't find 'Dog' as race, so...
   print mySecondChar.race.name  # => 'Human'


Class documentation
-------------------

.. automodule:: game.components.characters.races
   :members:
