Classes
=========

Description
-----------

a GameClass object (from now on just *Class*) represent one of the classes for a
character. These can be seen as *jobs* and every character has one.

A *Class* is used to specify lots of specific attributes and unique features
for each character and each :class:`Character` has one that can be both specified
or randomized.


Available Classes
-----------------

At the current stage of development the *classes* used for the game are the ones
described in the **Player's handbook 3.5**, so we have

* Barbarian
* Bard
* Cleric
* Druid
* Fighter
* Monk
* Ranger
* Rogue
* Sorcerer
* Wizard

Even though they are not fully implemented yet, they already have basic differences.

.. note::
   Due to the complexity of magic implementation, **Sorcerers** and **Wizards** may be
   soon removed from the list until magic can be used, as they would be powerless without it.


Class documentation
-------------------

.. automodule:: game.components.characters.gameClass
   :members:
