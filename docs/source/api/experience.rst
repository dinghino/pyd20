Experience
==========

The Experience objects are utility objects that are meant to handle the experience
of a :class:`Character` and their automatic levelling up when the required experience
for the next level is reached.

It has two major interface methods:

* ``gain`` that is used to give experience to the object (and the owner)
* ``lose`` that is used to remove experience.

There is a special property that allows you to know the percentage of experience
that the character has in the current level, as an *int* from 0 to 100,
reachable from ``percentage``. See example below.

Other methods and properties are stated in the *Class documentation* below


.. code-block:: python

   myCharacter = Character()
   print myCharacter
   # [Melisentia Blade][Elf | Wizard][1] | HP: 5/5
   print myCharacter.experience.current
   # 1 -> all Characters start at 1 experience point
   myCharacter.experience.gain(760)
   # Melisentia gained 760, that are not enough for level 2...
   print myCharacter.experience.percentage
   # 76 => she's at 76% of the exp required for the next level.
   myCharacter.experience.gain(300)
   # Melisentia gained enough to go to level 2, so...
   print myCharacter
   # [Melisentia Blade][Elf | Wizard][2] | HP: 7/7
   print myCharacter.experience.total
   # 1061 => total experience gained
   print myCharacter.experience.percentage
   # 3 => we've gone to level 2, so we have done 61 exp points in this level.
   #      2000 more are required for the next level, so we are at ~ 3%


Class documentation
-------------------

.. automodule:: game.components.characters.experience
   :members:
