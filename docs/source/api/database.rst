Database
========

Description
-----------

This module allows interaction with the ``sqlite3`` database created for the
game.
The database contains tables describing items, races, classes, level progression
and so on that are needed to build characters and items in the world and are
used by most of the classes in some way or another.

.. note::
   the sqlite database is still expanding as I'm working on converting the json
   data into sqlite tables, as well as finding a way to move data stored into
   the ``constants.py`` file in it (like the regex expressions), available
   abilities, races and game classes.

The module is very simple and provides semantic methods, meaning that every call
could be expressed as a phrase when creating it and most of the parameters are
optional too.


Module documentation
--------------------

.. automodule:: database
   :members:
