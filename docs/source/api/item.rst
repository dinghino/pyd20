Item
====

Description
-----------

``Item`` for now is the class that creates everything that has physical form
but is not a ``Character``. It will be subclassed into specific item types like
``Weapon`` or ``Armor``, ``Food`` and so on and will handle basic properties like

* Name
* Description
* value (price in coins)
* Weight
* Ownership (with ``owner`` property)
* ...

.. note::
   When the first subclasses will be created this page will be (re)moved
   and all its properties and methods will be included into the subclasses
   documentation.

Class documentation
-------------------

.. automodule:: game.components.utilities.item
   :members:

   .. :method: name
