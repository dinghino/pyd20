Purse
=====


Description
-----------

A ``Purse`` is an object that contains and handles money.

Money - for now at least- is virtually handled by strings and is divided in 4 type of coins:

* **Copper** coins
* **Silver** coins
* **Gold** coins
* **Platinum** coins

Each type is worth 10 of the previous one, so 1 gold is 10 silver.

Every ``Character`` has a coin purse that can be reached with the ``.purse`` property of the character.

.. note::
   For now the Purse object is quite a simple one and doesn't allow money conversion of any kind.
   This means that if, for example, a Character has to pay 10 GP but has 1 PP or 100 SP, which are the same
   The purse won't know how to do it and, virtually, the Character won't have money to do what it has to.

   This logic will be implemented in a near future and will be used to handle transaction between characters
   allowing handling change and paying with different coin types.

Example
-------

Using a purse is quite simple. You can instantiate a ``Purse`` object by itself, but usually is inside a container object such as a ``Character`` or an ``Item`` (a money bag, a crate or box...).

.. code-block:: python

   from game import Purse
   myPurse = Purse()

   myPurse.add('30 gp')  # Will add 30 gold coins to the purse
   print myPurse  # => 0 CP 0 SP 30 GP 0 PP

   bunchOfMoney = {'cp': 9, 'sp': 15}
   myPurse.addBunch(bunchOfMoney)
   print myPurse  # => 9 CP 15 SP 30 GP 0 PP

   myPurse.remove('15 gold')  # => will remove 30 GP. writing short or long name
                              # does not change the meaning



Class documentation
-------------------

.. automodule:: game.components.utilities.purse
   :members:
