Armory
======

Description
-----------

An ``Armory`` object has the primary function to create and assign items to
container, where a container is everything that has an :class:`game.components.utilities.inventory`
at its ``invetory`` property.

User interfaces are limited in number but semantic in concept, meaning that
it should be easy and logic to use them thinking phrases and using optional
paramters.

The Armory object interfaces with the database module to get the required data
from the sqlite database in order to validate and create the required objects.

.. note::
   For the sake of simplicity there should be only one Armory object for each
   game, allowing easier track of all the equipments generated.


Class Documentation
-------------------

.. automodule:: game.components.armory
   :members:
