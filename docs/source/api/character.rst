Character
=========

Description
-----------

Character are Player(s) and NPCs. They are intelligent being that can interact
with each other talking, trading, fighting and so on... All the characters of
the game are build using this class and can be randomly generated completely,
partially or with a specific set of properties that will allow a proper customization
of each character at it's level 1.

In the future We will have the ability to create characters passing a json object
that may or may not contain the initial data as well as the level progression for
each character. This will allow us to import and export characters easily.

Characters use lots of other classes to handle specific issues like health,
abilities, inventory or the money they carry. Refer to those classes documentation
to know how to interact with them directly, or use the interfaces in the Character
class to a more easy and natural interaction.


References
----------

Since a Character contains some of the other classes described in this documentation,
here is a list of them for a quick direct reference:

* Abilities (*coming soon*)
* Health :class:`game.components.utilities.health.Health`
* Experience :class:`game.components.characters.experience.Experience`
* Race :class:`game.components.characters.races.Race`
* GameClass :class:`game.components.characters.gameClass.GameClass`
* Inventory :class:`game.components.utilities.inventory.Inventory`
* Purse :class:`game.components.utilities.purse.Purse`



Character creation
------------------

Creating a Character is quite simple. You can specify no parameters for a full
random character or you can pass some properties you want it to have

.. code-block:: python

   from game import Character

   # Create a completely random character
   rndChar = Character()

   # Create a Male Human Fighter with first name Jonah, strength at 18 and
   # constitution at 16
   jonah = Character(
        gender='Male', race='human', gameClass='fighter', fname='Jonah',
        str=18, con=16)


Class Documentation
-------------------

.. automodule:: game.components.character
   :members:
   :show-inheritance:
   :inherited-members:
