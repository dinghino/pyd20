API references
==============
.. note::
   This is a temporary autogenerated page with the main classes of the game.
   Since I started using this documentation platform later on, I will update the
   docs while working on, updating and fixing stuff.
   I will also organize better the docs so that, for example, the characters base
   class is not visible but its methods and properties show in the Character class.

   I Will also try to organize them better so you will have properties separated
   from methods, especially for long classes like Character.

.. todo::

   A little note for myself.
   Create pages and docs for all the utility classes (Ability, Abilities,...)
   Create for other item's child classes (armor, etc)


.. toctree::
   :maxdepth: 1
   :caption: Character and character's handlers

   character
   health
   experience
   Playable races <race>
   Game Classes <gameClass>
   Money handler <purse>
   inventory


.. toctree::
   :maxdepth: 1
   :caption: Utility objects

   dice


.. toctree::
   :maxdepth: 1
   :caption: Items and equipments

   Item - base class <item>
   Armory - equipment handler <armory>
   weapon
   armor


.. toctree::
   :maxdepth: 1
   :caption: Extra utility modules

   database
