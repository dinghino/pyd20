Dice
====

Description
-----------

Since this is a ``D20`` system game, where the ``D`` stands for ``Dice``, they
are the primary concept of the game. They are used to randomize actions' result
and are used to do almost anything when playing the tabletop game.
Dices are named with a **d** followed by the number of faces of the dice itself,
so a ``d20`` is a dice with twenty faces. When we write ``1d20`` we mean that that action
requires to roll 1 d20.

Dices can also have bonuses applied to the roll, meaning that after you get the
roll result you add (or subract) the value of the bonus, so ``1d8+2`` means that
you roll 1 dice with 8 faces, then add 2 to the result, allowing your score to be
between 3 (1 + 2) and 10 (8 + 2).

One action or value, for example the damages of a :class:`Weapon` can do more than
one dice of damages, so we can do for example ``2d6+3``. This means that we roll
2 dices of 6 faces and THEN we add 3 to the result, allowing us to span from a
minimum result of 5 (1 + 1 + 3) to a maximum of 15 (6 + 6 + 3).

While in real life this is done by hand each time, This ``Dice`` class lets you
create a dice that contains all these parameters internally allowing you to execute
all the math internally like:

.. code-block:: python

   from game import Dice

   dice = Dice('2d6+3')
   dice.roll()  # roll 2d6+3 1 time
                # and return an integer between 5 and 15, i.e. '12'

   dice.roll(2)  # rolled the 2d6+3 2 times
                 # so result will be between 10 and 30



Default dices and game dices
----------------------------

In the tabletop game the default dices are **d4**, **d6**, **d8**, **d10**,
**d12**, **d20**, **d100**.

There are cases though where you may need to use different dices (for example for
smaller weapons where you need 1d2). In real life you would roll a real dice and then
do some math (for 1d2 you would roll a d4 and divide by 2). Since we are dealing
with numbers instead of physical dices we can actually create a dice of 2 faces
and use it.

Normally, whenever a dice is needed it is automatically created and used in place,
like when dealing damages with a weapon or rolling initiative (which requires a D20).


Class documentation
-------------------

.. automodule:: game.components.dice
   :members:
