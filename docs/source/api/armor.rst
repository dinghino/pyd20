Armors
======

Armors are protective gear that a character can use to enhance his or her defense
while, in most cases, having some maluses.
Similar to weapons, creation and assignment of armors are handled by an ``Armory``.

Class documentation
-------------------

Armor class extends from the Equipment subclass of :class:`game.components.utilities.item.Item`
that is used to specify equipable gear.

.. autoclass:: game.components.items.equipment.Armor
   :members:

Armor template
--------------

Each armor is described inside the ``armor`` table of the sqlite database and
has these properties.

.. code-block:: python

  {
      "name": "",           # <str> Name of the armor
      "slug":"",            # <str> Stripped name for reference, similar to web slugs
      "cost": "",           # <str> Price in "<qty> <coinType>" as in "15 GP"
      "description": "",    # <str> Description for the item
      "armor_class": 0,     # <int> Defense bonus of the armor
      "max_dexterity": 0,   # <int> Maximum dexterity bonus allowed when using the armor
      "armor_type": ""      # <str> What type of armor is it (Light, Medium...)
  }
