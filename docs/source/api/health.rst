Health
======

Description
-----------

Health objects are used to handle the *health points* of a Character, meaning
they represent how much damage they can take before falling and then die.

in a :class:`Character` they are stored inside the ``hp`` property that can
be accessed to get the ``current`` or ``total`` amount of health points.


Class documentation
-------------------

.. automodule:: game.components.utilities.health
   :members:
