.. PyD20 documentation master file, created by
   sphinx-quickstart on Tue Aug 30 12:20:52 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

PyD20 Index
===========

PyD20 is a project of indefinite size that I started building to learn and improve my Python skills.
The final goal of the project is to have an rpg of some sort that uses more rules as possible from D&D 3.5, while keeping it reasonably complex.

For now I'm handling the basic logic for character creation, inventory and money handling and basic actions, but there is no way to create a game (apart from the ``Combat`` class) and interact with it once it's started.

.. toctree::
   :maxdepth: 2
   :caption: Documentation index

   api/index
   license
   help


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
