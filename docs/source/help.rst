Help
====
I am learning, so If you are reading this you may probably know more than me about your questions but you can contact me any time for anything and I will try to answer as best I could.
If your question is something like "why you did ``this way`` instead of ``that way``?" the answer is that I probably didn't (or still don't) know ``that way`` or how to implement it :)

Contacts
========
`Git Repo <http://github.com/dinghino/pyd20>`_ ~ email_


.. _email: dinghino@gmail.com
