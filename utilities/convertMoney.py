"""Utility function that can convert coins from one type to another, evaluating
a simple change if needed.
Will be moved inside a proper class (like a Character.Banker class or inside
Purse if found appropriate) in the future. """


def convertMoney(FROM, TO, qty=1):
    """Convert <qty> <FROM> coins into <TO> coins if possible. """

    # Find the coins type into MONEY_TYPE
    fromType = Purse._findMoneyType(FROM)
    toType = Purse._findMoneyType(TO)


    # Check that the money type exist
    if not fromType or not toType:
        raise TypeError('Could\'t find %s or %s as money' % FROM, TO)

    # We can't convert gold in gold, can we..?
    if fromType == toType:
        raise ValueError('Cannot convert to the same type of coins')

    # Find the positional index of the types, so we know how 'far' they are
    fromIdx = MONEY_TYPE.index(fromType)
    toIdx = MONEY_TYPE.index(toType)

    # Calculate the conversion rate
    factor = 10 ** abs(fromIdx - toIdx)

    if toIdx < fromIdx:
        # We are converting from a higher value coin to a lower one
        toCoins = qty * factor
        fromCoins = 0
    else:
        # we are going from a lower value to a higher value. we need some cash
        # to do so.
        if qty < factor:
            print 'Can\'t convert less than %s %s coins into %s' % \
                  (factor, fromType[0], toType[0])

            return 0, 0
        else:
            toCoins = int(qty / factor)
            fromCoins = qty % factor

    return toCoins, fromCoins
